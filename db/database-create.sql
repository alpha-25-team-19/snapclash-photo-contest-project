create or replace table photo_contest.contest_categories
(
    category_id int auto_increment
        primary key,
    category_name varchar(50) not null,
    constraint contest_categories_category_name_uindex
        unique (category_name)
);

create or replace table photo_contest.contest_types
(
    type_id int auto_increment
        primary key,
    type_name varchar(50) not null,
    constraint contest_types_type_name_uindex
        unique (type_name)
);

create or replace table photo_contest.contests
(
    contest_id int auto_increment
        primary key,
    title varchar(50) not null,
    category_id int null,
    phase_one_start datetime not null,
    phase_one_end_phase_two_start datetime not null,
    phase_two_end datetime not null,
    type_id int null,
    isAwarded tinyint(1) null,
    constraint contests_title_uindex
        unique (title),
    constraint contests_contest_categories_fk
        foreign key (category_id) references photo_contest.contest_categories (category_id),
    constraint contests_contest_types_fk
        foreign key (type_id) references photo_contest.contest_types (type_id)
);

create or replace table photo_contest.rankings
(
    ranking_id int auto_increment
        primary key,
    total_score int null
);

create or replace table photo_contest.roles
(
    role_id int auto_increment
        primary key,
    role_name varchar(50) not null,
    constraint roles_role_name_uindex
        unique (role_name)
);

create or replace table photo_contest.users
(
    user_id int auto_increment
        primary key,
    first_name varchar(50) not null,
    last_name varchar(50) not null,
    username varchar(50) not null,
    password varchar(30) not null,
    constraint users_username_uindex
        unique (username)
);

create or replace table photo_contest.contest_jurors
(
    contest_id int null,
    user_id int null,
    constraint contest_jurors_contests_fk
        foreign key (contest_id) references photo_contest.contests (contest_id),
    constraint contest_jurors_users_fk
        foreign key (user_id) references photo_contest.users (user_id)
);

create or replace table photo_contest.contest_users_invited
(
    contest_id int null,
    user_id int null,
    constraint contest_users_invited_contests_fk
        foreign key (contest_id) references photo_contest.contests (contest_id),
    constraint contest_users_invited_users_fk
        foreign key (user_id) references photo_contest.users (user_id)
);

create or replace table photo_contest.photos
(
    photo_id int auto_increment
        primary key,
    title varchar(50) not null,
    story text not null,
    user_id int null,
    constraint photos_users_fk
        foreign key (user_id) references photo_contest.users (user_id)
);

create or replace table photo_contest.contest_photos
(
    photo_id int null,
    contest_id int null,
    constraint contest_photos_contests_fk
        foreign key (contest_id) references photo_contest.contests (contest_id),
    constraint contest_photos_photos_fk
        foreign key (photo_id) references photo_contest.photos (photo_id)
);

create or replace table photo_contest.photo_reviews
(
    score_id int auto_increment
        primary key,
    photo_id int null,
    score tinyint not null,
    juror_id int null,
    isInvalidCategory tinyint(1) null,
    comment text null,
    constraint photo_scores_photos_fk
        foreign key (photo_id) references photo_contest.photos (photo_id),
    constraint photo_scores_users_fk
        foreign key (juror_id) references photo_contest.users (user_id)
);

create or replace table photo_contest.photo_urls
(
    url_id int auto_increment
        primary key,
    photo_url varchar(250) null,
    photo_id int null,
    constraint photo_urls_photos_fk
        foreign key (photo_id) references photo_contest.photos (photo_id)
);

create or replace table photo_contest.users_rankings
(
    user_id int not null,
    ranking_id int not null,
    constraint users_rankings_rankings_fk
        foreign key (ranking_id) references photo_contest.rankings (ranking_id),
    constraint users_rankings_users_fk
        foreign key (user_id) references photo_contest.users (user_id)
);

create or replace table photo_contest.users_roles
(
    user_id int null,
    role_id int null,
    constraint users_roles_roles_fk
        foreign key (role_id) references photo_contest.roles (role_id),
    constraint users_roles_users_fk
        foreign key (user_id) references photo_contest.users (user_id)
);

