package com.telerikacademy.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.HashMap;

@ApiModel("Class representing a contest's leaderboard in the application")
public class ContestLeaderboard {

    @JsonIgnore
    @ApiModelProperty("Contest, for which a leaderboard will be created")
    private Contest contest;

    @ApiModelProperty("List, where the participants qualified on first place in the contest, will be stored")
    private HashMap<String, Double> firstPlaceWinners;

    @ApiModelProperty("List, where the participants qualified on second place in the contest, will be stored")
    private HashMap<String, Double> secondPlaceWinners;

    @ApiModelProperty("List, where the participants qualified on third place in the contest, will be stored")
    private HashMap<String, Double> thirdPlaceWinners;

    public ContestLeaderboard() {
    }

    public ContestLeaderboard(Contest contest, HashMap<String, Double> firstPlaceWinners,
                              HashMap<String, Double> secondPlaceWinners,
                              HashMap<String, Double> thirdPlaceWinners) {
        this.contest = contest;
        this.firstPlaceWinners = firstPlaceWinners;
        this.secondPlaceWinners = secondPlaceWinners;
        this.thirdPlaceWinners = thirdPlaceWinners;
    }

    public HashMap<String, Double> getFirstPlaceWinners() {
        return firstPlaceWinners;
    }

    public void setFirstPlaceWinners(HashMap<String, Double> firstPlaceWinners) {
        this.firstPlaceWinners = firstPlaceWinners;
    }

    public HashMap<String, Double> getSecondPlaceWinners() {
        return secondPlaceWinners;
    }

    public void setSecondPlaceWinners(HashMap<String, Double> secondPlaceWinners) {
        this.secondPlaceWinners = secondPlaceWinners;
    }

    public HashMap<String, Double> getThirdPlaceWinners() {
        return thirdPlaceWinners;
    }

    public void setThirdPlaceWinners(HashMap<String, Double> thirdPlaceWinners) {
        this.thirdPlaceWinners = thirdPlaceWinners;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public String getContestInfo() {
        return String.format(
                "'%s', Category: %s, Type: %s",
                getContest().getTitle(),
                getContest().getContestCategory().getCategoryName(),
                getContest().getContestType().getTypeName());
    }
}
