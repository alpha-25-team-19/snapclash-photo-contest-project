package com.telerikacademy.photocontest.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@ApiModel("Class representing a photo URL in the application")
@Entity
@Table(name = "photo_urls")
public class PhotoUrl {

    @ApiModelProperty("Unique identifier of the Photo URL")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "url_id")
    private int id;

    @ApiModelProperty("URL of the Photo URL")
    @Column(name = "photo_url")
    private String photoUrl;

    @OneToOne
    @JoinColumn(name = "photo_id")
    private Photo photo;

    public PhotoUrl() {
    }

    public PhotoUrl(int id, String photoUrl, Photo photo) {
        this.id = id;
        this.photoUrl = photoUrl;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }
}
