package com.telerikacademy.photocontest.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.telerikacademy.photocontest.models.enums.ContestPhase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@ApiModel("Class representing a contest in the application")
@Entity
@Table(name = "contests")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Contest {

    @ApiModelProperty("Unique identifier of the Contest")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int id;

    @ApiModelProperty("Title of the Contest")
    @Column(name = "title")
    private String title;

    @ApiModelProperty("Category of the Contest")
    @ManyToOne
    @JoinColumn(name = "category_id")
    private ContestCategory contestCategory;

    @ApiModelProperty("Type of Contest")
    @ManyToOne
    @JoinColumn(name = "type_id")
    private ContestType contestType;

    @ApiModelProperty("Start date and time of Phase I of the Contest")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "phase_one_start")
    private LocalDateTime phaseOneStart;

    @ApiModelProperty("End date and time of Phase I and start date and time of Phase II of the Contest")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "phase_one_end_phase_two_start")
    private LocalDateTime phaseTwoStart;

    @ApiModelProperty("End date and time of Phase II of the Contest")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "phase_two_end")
    private LocalDateTime contestEnd;

    @ApiModelProperty("List of the Users in the jury of the Contest")
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_jurors",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> jury;

    @ApiModelProperty("List of Users invited to participate in the Contest, if type is 'Invitational'")
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_users_invited",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> invitedUsers;

    @ApiModelProperty("Boolean representing is the contest already awarded")
    @JsonIgnore
    @Column(name = "isAwarded")
    private boolean isAwarded;

    public Contest(int id,
                   String title,
                   ContestCategory contestCategory,
                   ContestType contestType,
                   LocalDateTime phaseOneStart,
                   LocalDateTime phaseTwoStart,
                   LocalDateTime contestEnd,
                   Set<User> jury,
                   Set<User> invitedUsers,
                   boolean isAwarded) {
        this.id = id;
        this.title = title;
        this.contestCategory = contestCategory;
        this.contestType = contestType;
        this.phaseOneStart = phaseOneStart;
        this.phaseTwoStart = phaseTwoStart;
        this.contestEnd = contestEnd;
        this.jury = jury;
        this.invitedUsers = invitedUsers;
        this.isAwarded = isAwarded;
    }

    public Contest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ContestCategory getContestCategory() {
        return contestCategory;
    }

    public void setContestCategory(ContestCategory contestCategory) {
        this.contestCategory = contestCategory;
    }

    public ContestType getContestType() {
        return contestType;
    }

    public void setContestType(ContestType contestType) {
        this.contestType = contestType;
    }

    public String getContestPhase() {
        if (phaseOneStart.isAfter(LocalDateTime.now())) {
            return ContestPhase.NOT_STARTED.toString();
        }
        if (phaseOneStart.isBefore(LocalDateTime.now()) && phaseTwoStart.isAfter(LocalDateTime.now())) {
            return ContestPhase.PHASE_ONE.toString();
        }
        if (phaseTwoStart.isBefore(LocalDateTime.now()) && contestEnd.isAfter(LocalDateTime.now())) {
            return ContestPhase.PHASE_TWO.toString();
        }
        return ContestPhase.FINISHED.toString();
    }

    public LocalDateTime getPhaseOneStart() {
        return phaseOneStart;
    }

    public void setPhaseOneStart(LocalDateTime phaseOneStart) {
        this.phaseOneStart = phaseOneStart;
    }

    public LocalDateTime getPhaseTwoStart() {
        return phaseTwoStart;
    }

    public void setPhaseTwoStart(LocalDateTime phaseTwoStart) {
        this.phaseTwoStart = phaseTwoStart;
    }

    public LocalDateTime getContestEnd() {
        return contestEnd;
    }

    public void setContestEnd(LocalDateTime contestEnd) {
        this.contestEnd = contestEnd;
    }

    public Set<User> getJury() {
        return jury;
    }

    public void setJury(Set<User> jury) {
        this.jury = jury;
    }

    public Set<User> getInvitedUsers() {
        return invitedUsers;
    }

    public void setInvitedUsers(Set<User> invitedUsers) {
        this.invitedUsers = invitedUsers;
    }

    public boolean isAwarded() {
        return isAwarded;
    }

    public void setAwarded(boolean awarded) {
        isAwarded = awarded;
    }
}
