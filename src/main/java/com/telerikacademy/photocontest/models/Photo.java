package com.telerikacademy.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Objects;

@ApiModel("Class representing a photo in the application")
@Entity
@Table(name = "photos")
public class Photo {

    @ApiModelProperty("Unique identifier of the Photo")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private int id;

    @ApiModelProperty("Title of the Photo")
    @Column(name = "title")
    private String title;

    @ApiModelProperty("Story / Description of the Photo")
    @Column(name = "story")
    private String story;

    @ApiModelProperty("User who submitted the Photo")
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User photoUser;

    @JsonIgnore
    @ApiModelProperty("Contest to which the Photo is submitted")
    @ManyToOne
    @JoinTable(
            name = "contest_photos",
            joinColumns = @JoinColumn(name = "photo_id"),
            inverseJoinColumns = @JoinColumn(name = "contest_id")
    )
    private Contest contest;


    public Photo() {
    }

    public Photo(int id,
                 String title,
                 String story,
                 User photoUser,
                 Contest contest) {
        this.id = id;
        this.title = title;
        this.story = story;
        this.photoUser = photoUser;
        this.contest = contest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public User getPhotoUser() {
        return photoUser;
    }

    public void setPhotoUser(User photoUser) {
        this.photoUser = photoUser;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public String getContestInfo() {
        return String.format("'%s', Category: %s", getContest().getTitle(), getContest().getContestCategory().getCategoryName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return getId() == photo.getId() && getTitle().equals(photo.getTitle()) && getStory().equals(photo.getStory()) && getPhotoUser().equals(photo.getPhotoUser()) && getContest().equals(photo.getContest());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getStory(), getPhotoUser(), getContest());
    }
}

