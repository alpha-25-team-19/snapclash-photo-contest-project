package com.telerikacademy.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

import java.util.Objects;


@ApiModel("Class representing a user in the application")
@Entity
@Table(name = "users")
public class User {

    @ApiModelProperty("Unique identifier of the User")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @ApiModelProperty("Username of the User")
    @Column(name = "username")
    private String username;

    @ApiModelProperty("Password of the User")
    @Column(name = "password")
    @JsonIgnore
    private String password;

    @ApiModelProperty("First name of the User")
    @Column(name = "first_name")
    private String firstName;

    @ApiModelProperty("Last name of the User")
    @Column(name = "last_name")
    private String lastName;

    @ApiModelProperty("Role of the User in the application")
    @ManyToOne
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Role role;

    public User() {
    }

    public User(int id, String username, String password,
                String firstName, String lastName,
                Role role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return getId() == user.getId() && getUsername().equals(user.getUsername())
                && getPassword().equals(user.getPassword())
                && getFirstName().equals(user.getFirstName())
                && getLastName().equals(user.getLastName())
                && getRole().equals(user.getRole());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUsername(), getPassword(), getFirstName(), getLastName(), getRole());
    }
}
