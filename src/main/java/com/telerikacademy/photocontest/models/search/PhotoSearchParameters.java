package com.telerikacademy.photocontest.models.search;

public class PhotoSearchParameters {

    private String contest;

    private String username;

    public PhotoSearchParameters() {
    }

    public PhotoSearchParameters(String contest, String username) {
        this.contest = contest;
        this.username = username;
    }

    public String getContest() {
        return contest;
    }

    public void setContest(String contest) {
        this.contest = contest;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
