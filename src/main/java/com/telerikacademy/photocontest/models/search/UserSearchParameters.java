package com.telerikacademy.photocontest.models.search;

public class UserSearchParameters {

    private String name;

    private String ranking;

    public UserSearchParameters(String name, String ranking) {
        this.ranking = ranking;
        this.name = name;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
