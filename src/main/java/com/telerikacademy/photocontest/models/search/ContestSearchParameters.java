package com.telerikacademy.photocontest.models.search;

public class ContestSearchParameters {

    private String phase;

    private String title;

    private String contestType;

    private String contestCategory;

    private Integer userId;

    public ContestSearchParameters(String phase,
                                   String title,
                                   String contestType,
                                   String contestCategory,
                                   Integer userId) {
        this.phase = phase;
        this.title = title;
        this.contestType = contestType;
        this.contestCategory = contestCategory;
        this.userId = userId;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContestType() {
        return contestType;
    }

    public void setContestType(String contestType) {
        this.contestType = contestType;
    }

    public String getContestCategory() {
        return contestCategory;
    }

    public void setContestCategory(String contestCategory) {
        this.contestCategory = contestCategory;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
