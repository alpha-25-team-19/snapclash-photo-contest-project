package com.telerikacademy.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.photocontest.models.enums.RankType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@ApiModel("Class representing a user's ranking in the application")
@Entity
@Table(name = "rankings")
public class Ranking {

    @ApiModelProperty("Unique identifier of the ranking")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ranking_id")
    @JsonIgnore
    private int id;

    @ApiModelProperty("Total score of the User with this Ranking")
    @Column(name = "total_score")
    private int totalScore;

    @ApiModelProperty("The User with this Ranking")
    @OneToOne
    @JoinTable(
            name = "users_rankings",
            joinColumns = @JoinColumn(name = "ranking_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private User user;

    public Ranking() {
    }

    public Ranking(int id, int totalScore) {
        this.id = id;
        this.totalScore = totalScore;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public String getRankType() {
        if (getUser().getRole().getRoleName().equals(ORGANISER_ROLE)) {
            return ORGANISER_ROLE;
        }

        if (getTotalScore() >= DEFAULT_USER_SCORE && getTotalScore() < PHOTO_ENTHUSIAST_MIN_SCORE) {
            return RankType.PHOTO_JUNKIE.toString();
        }
        if (getTotalScore() >= PHOTO_ENTHUSIAST_MIN_SCORE && getTotalScore() < PHOTO_MASTER_MIN_SCORE) {
            return RankType.PHOTO_ENTHUSIAST.toString();
        }
        if (getTotalScore() >= PHOTO_MASTER_MIN_SCORE && getTotalScore() < PHOTO_DICTATOR_MIN_SCORE) {
            return RankType.PHOTO_MASTER.toString();
        }
        return RankType.WISE_AND_BENEVOLENT_PHOTO_DICTATOR.toString();
    }

    public String getPointsUntilNextRank() {
        if (getTotalScore() < PHOTO_ENTHUSIAST_MIN_SCORE) {
            return String.valueOf(PHOTO_ENTHUSIAST_MIN_SCORE - getTotalScore());
        } else if (getTotalScore() < PHOTO_MASTER_MIN_SCORE) {
            return String.valueOf(PHOTO_MASTER_MIN_SCORE - getTotalScore());
        } else if (getTotalScore() < PHOTO_DICTATOR_MIN_SCORE) {
            return String.valueOf(PHOTO_DICTATOR_MIN_SCORE - getTotalScore());
        } else {
            return MAX_USER_RANKING;
        }
    }


}
