package com.telerikacademy.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@ApiModel("Class representing a contest's category in the application")
@Entity
@Table(name = "contest_categories")
public class ContestCategory {

    @ApiModelProperty("Unique identifier of the Contest Category")
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private int id;

    @ApiModelProperty("Name of the Contest Category")
    @NotBlank
    @Column(name = "category_name")
    private String categoryName;

    public ContestCategory() {
    }

    public ContestCategory(int id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
