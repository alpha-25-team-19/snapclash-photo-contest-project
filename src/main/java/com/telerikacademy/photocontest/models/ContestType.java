package com.telerikacademy.photocontest.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@ApiModel("Class representing a contest's type in the application")
@Entity
@Table(name = "contest_types")
public class ContestType {

    @ApiModelProperty("Unique identifier of the Contest Type")
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private int id;

    @ApiModelProperty("Name of the Contest Type")
    @NotBlank
    @Column(name = "type_name")
    private String typeName;

    public ContestType(int id, String typeName) {
        this.id = id;
        this.typeName = typeName;
    }

    public ContestType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
