package com.telerikacademy.photocontest.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@ApiModel("Class representing a photo review in the application")
@Entity
@Table(name = "photo_reviews")
public class PhotoReview {

    @ApiModelProperty("Unique identifier of the Photo Review")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "score_id")
    private int id;

    @ApiModelProperty("User who submitted the Photo Review")
    @OneToOne
    @JoinColumn(name = "juror_id")
    private User juror;

    @ApiModelProperty("Photo reviewed in the Photo Review")
    @ManyToOne
    @JoinColumn(name = "photo_id")
    private Photo photo;

    @ApiModelProperty("Score assigned by the Juror to the Photo in the Photo Review")
    @Column(name = "score")
    private int photoScore;

    @ApiModelProperty("A comment from the Juror for the Photo scored in the Photo review")
    @Column(name = "comment")
    private String photoComment;

    @ApiModelProperty("Boolean representing if the Juror thought the Photo does not fit the Category of the Contest it was submitted to")
    @Column(name = "isInvalidCategory")
    private boolean isInvalidCategory;

    public PhotoReview() {
    }

    public PhotoReview(int id,
                       User juror,
                       Photo photo,
                       int photoScore,
                       String photoComment,
                       boolean isInvalidCategory) {
        this.id = id;
        this.juror = juror;
        this.photo = photo;
        this.photoScore = photoScore;
        this.photoComment = photoComment;
        this.isInvalidCategory = isInvalidCategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getJuror() {
        return juror;
    }

    public void setJuror(User juror) {
        this.juror = juror;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public int getPhotoScore() {
        return photoScore;
    }

    public void setPhotoScore(int photoScore) {
        this.photoScore = photoScore;
    }

    public String getPhotoComment() {
        return photoComment;
    }

    public void setPhotoComment(String photoComment) {
        this.photoComment = photoComment;
    }

    public boolean isInvalidCategory() {
        return isInvalidCategory;
    }

    public void setInvalidCategory(boolean invalidCategory) {
        isInvalidCategory = invalidCategory;
    }
}
