package com.telerikacademy.photocontest.models.enums;

public enum ContestPhase {

    NOT_STARTED,
    PHASE_ONE,
    PHASE_TWO,
    FINISHED;

    @Override
    public String toString() {
        switch (this) {
            case NOT_STARTED:
                return "Not started yet";
            case PHASE_ONE:
                return "Phase one";
            case PHASE_TWO:
                return "Phase two";
            case FINISHED:
                return "Finished";
            default:
                return "";
        }
    }

}
