package com.telerikacademy.photocontest.models.enums;

public enum RankType {
    PHOTO_JUNKIE,
    PHOTO_ENTHUSIAST,
    PHOTO_MASTER,
    WISE_AND_BENEVOLENT_PHOTO_DICTATOR;

    @Override
    public String toString() {
        switch (this) {
            case PHOTO_JUNKIE:
                return "Photo Junkie";
            case PHOTO_ENTHUSIAST:
                return "Photo Enthusiast";
            case PHOTO_MASTER:
                return "Photo Master";
            case WISE_AND_BENEVOLENT_PHOTO_DICTATOR:
                return "Wise and Benevolent Photo Dictator";
            default:
                return "";
        }
    }
}
