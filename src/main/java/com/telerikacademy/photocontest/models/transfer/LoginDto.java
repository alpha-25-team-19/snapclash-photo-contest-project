package com.telerikacademy.photocontest.models.transfer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

public class LoginDto {

    @Size(min = MIN_USER_USERNAME_LENGTH, max = MAX_USER_USERNAME_LENGTH, message = USER_USERNAME_LENGTH_RANGE_MESSAGE)
    @NotEmpty
    private String username;

    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH, message = USER_PASSWORD_LENGTH_RANGE_MESSAGE)
    @NotEmpty
    private String password;

    public LoginDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public LoginDto() {
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
