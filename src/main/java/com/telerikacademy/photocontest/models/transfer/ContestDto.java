package com.telerikacademy.photocontest.models.transfer;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

public class ContestDto {

    @NotNull
    @Size(min = MIN_CONTEST_TITLE_LENGTH,
            max = MAX_CONTEST_TITLE_LENGTH,
            message = CONTEST_TITLE_LENGTH_RANGE_MESSAGE)
    private String title;

    @NotNull
    @Positive
    private int contestCategoryId;

    @Future(message = START_DATE_PHASE_ONE_VALIDATION_MESSAGE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime phaseOneStart;

    @Future(message = END_DATE_PHASE_ONE_VALIDATION_MESSAGE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime phaseTwoStart;

    @Future(message = END_DATE_PHASE_TWO_VALIDATION_MESSAGE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime contestEnd;

    @NotEmpty
    private ArrayList<Integer> juryIds;

    private ArrayList<Integer> invitedUsersIds;

    public ContestDto(String title,
                      int contestCategoryId,
                      LocalDateTime phaseOneStart,
                      LocalDateTime phaseTwoStart,
                      LocalDateTime contestEnd,
                      ArrayList<Integer> juryIds,
                      ArrayList<Integer> invitedUsersIds) {
        this.title = title;
        this.contestCategoryId = contestCategoryId;
        this.phaseOneStart = phaseOneStart;
        this.phaseTwoStart = phaseTwoStart;
        this.contestEnd = contestEnd;
        this.juryIds = juryIds;
        this.invitedUsersIds = invitedUsersIds;
    }

    public ContestDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getContestCategoryId() {
        return contestCategoryId;
    }

    public void setContestCategoryId(int contestCategoryId) {
        this.contestCategoryId = contestCategoryId;
    }

    public LocalDateTime getPhaseOneStart() {
        return phaseOneStart;
    }

    public void setPhaseOneStart(LocalDateTime phaseOneStart) {
        this.phaseOneStart = phaseOneStart;
    }

    public LocalDateTime getPhaseTwoStart() {
        return phaseTwoStart;
    }

    public void setPhaseTwoStart(LocalDateTime phaseTwoStart) {
        this.phaseTwoStart = phaseTwoStart;
    }

    public LocalDateTime getContestEnd() {
        return contestEnd;
    }

    public void setContestEnd(LocalDateTime contestEnd) {
        this.contestEnd = contestEnd;
    }

    public ArrayList<Integer> getJuryIds() {
        return juryIds;
    }

    public void setJuryIds(ArrayList<Integer> juryIds) {
        this.juryIds = juryIds;
    }

    public ArrayList<Integer> getInvitedUsersIds() {
        return invitedUsersIds;
    }

    public void setInvitedUsersIds(ArrayList<Integer> invitedUsersIds) {
        this.invitedUsersIds = invitedUsersIds;
    }
}
