package com.telerikacademy.photocontest.models.transfer;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@ApiModel("Class representing a user data transfer object for creating users in the application")
public class UserDto {


    @Size(min = MIN_USER_USERNAME_LENGTH, max = MAX_USER_USERNAME_LENGTH, message = USER_USERNAME_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String username;

    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH, message = USER_PASSWORD_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String password;

    @Size(min = MIN_USER_NAME_LENGTH, max = MAX_USER_NAME_LENGTH, message = USER_FIRST_NAME_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String firstName;

    @Size(min = MIN_USER_NAME_LENGTH, max = MAX_USER_NAME_LENGTH, message = USER_LAST_NAME_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String lastName;

    public UserDto(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
