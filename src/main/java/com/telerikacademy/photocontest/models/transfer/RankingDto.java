package com.telerikacademy.photocontest.models.transfer;

import javax.validation.constraints.Positive;

public class RankingDto {

    @Positive
    private Integer newTotalScore;

    @Positive
    private Integer pointsToAdd;

    @Positive
    private Integer pointsToRemove;

    public RankingDto() {
    }

    public RankingDto(Integer newTotalScore, Integer pointsToAdd, Integer pointsToRemove) {
        this.newTotalScore = newTotalScore;
        this.pointsToAdd = pointsToAdd;
        this.pointsToRemove = pointsToRemove;
    }

    public Integer getNewTotalScore() {
        return newTotalScore;
    }

    public void setNewTotalScore(Integer newTotalScore) {
        this.newTotalScore = newTotalScore;
    }

    public Integer getPointsToAdd() {
        return pointsToAdd;
    }

    public void setPointsToAdd(Integer pointsToAdd) {
        this.pointsToAdd = pointsToAdd;
    }

    public Integer getPointsToRemove() {
        return pointsToRemove;
    }

    public void setPointsToRemove(Integer pointsToRemove) {
        this.pointsToRemove = pointsToRemove;
    }
}
