package com.telerikacademy.photocontest.models.transfer;

import io.swagger.annotations.ApiModel;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@ApiModel("Class representing a photo review data transfer object in the application")
public class PhotoReviewDto {

    @Positive(message = JUROR_ID_POSITIVE)
    private int jurorId;

    @Positive(message = PHOTO_ID_POSITIVE)
    private int photoId;

    @Range(min = MIN_PHOTO_SCORE, max = MAX_PHOTO_SCORE, message = PHOTO_SCORE_RANGE_MESSAGE)
    private int photoScore;

    @NotBlank(message = PHOTO_REVIEW_COMMENT_NOT_BLANK)
    @Length(max = MAX_PHOTO_COMMENT_SIZE, message = PHOTO_COMMENT_MAX_LENGTH_MESSAGE)
    private String comment;

    private boolean isInvalidCategory;

    public PhotoReviewDto(int jurorId, int photoId, int photoScore, String comment, boolean isInvalidCategory) {
        this.jurorId = jurorId;
        this.photoId = photoId;
        this.photoScore = photoScore;
        this.comment = comment;
        this.isInvalidCategory = isInvalidCategory;
    }

    public PhotoReviewDto() {
    }

    public int getJurorId() {
        return jurorId;
    }

    public void setJurorId(int jurorId) {
        this.jurorId = jurorId;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public int getPhotoScore() {
        return photoScore;
    }

    public void setPhotoScore(int photoScore) {
        this.photoScore = photoScore;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isInvalidCategory() {
        return isInvalidCategory;
    }

    public void setInvalidCategory(boolean invalidCategory) {
        isInvalidCategory = invalidCategory;
    }
}
