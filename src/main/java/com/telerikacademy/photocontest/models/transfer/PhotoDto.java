package com.telerikacademy.photocontest.models.transfer;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@ApiModel("Class representing a photo data transfer object in the application")
public class PhotoDto {

    @Size(min = MIN_PHOTO_TITLE_LENGTH, max = MAX_PHOTO_TITLE_LENGTH, message = PHOTO_TITLE_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String title;

    @Size(min = MIN_PHOTO_STORY_LENGTH, max = MAX_PHOTO_STORY_LENGTH, message = PHOTO_STORY_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String story;

    @NotNull
    private int userId;

    @Positive(message = CONTEST_ID_POSITIVE)
    private int contestId;

    public PhotoDto() {
    }

    public PhotoDto(String title, String story, int userId, int contestId) {
        this.title = title;
        this.story = story;
        this.userId = userId;
        this.contestId = contestId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }
}
