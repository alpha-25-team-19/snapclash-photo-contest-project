package com.telerikacademy.photocontest.models.transfer;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@ApiModel("Class representing a user data transfer object for updating users in the application")
public class UpdateUserDto {

    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH, message = USER_PASSWORD_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String password;

    @Size(min = MIN_USER_NAME_LENGTH, max = MAX_USER_NAME_LENGTH, message = USER_FIRST_NAME_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String firstName;

    @Size(min = MIN_USER_NAME_LENGTH, max = MAX_USER_NAME_LENGTH, message = USER_LAST_NAME_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String lastName;

    private boolean isPromotedToOrganiser;

    public UpdateUserDto() {
    }

    public UpdateUserDto(String password,
                         String firstName,
                         String lastName,
                         boolean isPromotedToOrganiser) {
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isPromotedToOrganiser = isPromotedToOrganiser;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isPromotedToOrganiser() {
        return isPromotedToOrganiser;
    }

    public void setPromotedToOrganiser(boolean promotedToOrganiser) {
        isPromotedToOrganiser = promotedToOrganiser;
    }
}
