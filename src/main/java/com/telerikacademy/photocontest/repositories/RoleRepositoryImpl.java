package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Role;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class RoleRepositoryImpl extends AbstractGenericGetRepository<Role> implements RoleRepository {

    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Role> getAll() {
        return super.getAll(Role.class);
    }

    @Override
    public Role getById(int id) {
        return super.getByField("id", id, Role.class);
    }

    @Override
    public Role getByName(String roleName) {
        return super.getByField("roleName", roleName, Role.class);
    }
}
