package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.search.UserSearchParameters;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;
import static com.telerikacademy.photocontest.utils.QueryHelpers.like;

@Repository
public class UserRepositoryImpl extends AbstractGenericGetRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        return super.getAll(User.class);
    }

    @Override
    public List<User> getAllOrganisers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where role.id = 2", User.class);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getAllEligibleJurors() {
        try (Session session = sessionFactory.openSession()) {
            Query<Ranking> query = session.createQuery("from Ranking  where totalScore >150", Ranking.class);
            List<Ranking> jurorRankings = query.getResultList();
            List<User> eligibleJurors = new ArrayList<>();
            for (Ranking ranking : jurorRankings) {
                eligibleJurors.add(ranking.getUser());
            }
            return eligibleJurors;
        }
    }

    @Override
    public User getById(int id) {
        return super.getByField("id", id, User.class);
    }

    @Override
    public User getByUsername(String username) {
        return super.getByField("username", username, User.class);
    }

    @Override
    public List<Ranking> filter(UserSearchParameters userSearchParameters) {
        try (Session session = sessionFactory.openSession()) {

            StringBuilder baseQuery = new StringBuilder(" from Ranking where 1=1");
            var filters = new ArrayList<String>();
            generateQuery(userSearchParameters, baseQuery, filters);

            Query<Ranking> query = session.createQuery(baseQuery.toString(), Ranking.class);

            if (!userSearchParameters.getName().isBlank()) {
                query.setParameter("name", like(userSearchParameters.getName()));
            }

            if (query.getResultList().size() == 0) {
                throw new EntityNotFoundException(NO_RESULTS_FOUND);
            }

            return query.list();
        }
    }

    private void generateQuery(UserSearchParameters userSearchParameters, StringBuilder baseQuery, ArrayList<String> filters) {
        if (!userSearchParameters.getName().isBlank()) {
            filters.add(" AND user.username like lower(:name) " +
                    "OR user.firstName like lower(:name) " +
                    "OR user.lastName like lower(:name) ");
        }

        if (!userSearchParameters.getRanking().isBlank()) {
            switch (userSearchParameters.getRanking()) {
                case PHOTO_JUNKIE:
                    filters.add(" AND totalScore >= 0 AND totalScore <= 50 ");
                    break;
                case PHOTO_ENTHUSIAST:
                    filters.add(" AND totalScore >= 51 AND totalScore <= 150 ");
                    break;
                case PHOTO_MASTER:
                    filters.add(" AND totalScore >= 151 AND totalScore <= 1000 ");
                    break;
                case PHOTO_DICTATOR:
                    filters.add(" AND totalScore > 1000 ");
                    break;
                default:
                    throw new EntityNotFoundException(
                            String.format(INVALID_USER_RANKING, userSearchParameters.getRanking()));
            }
        }


        filters.add(" order by totalScore desc ");


        if (filters.size() > 0) {
            for (String filter : filters) {
                baseQuery.append(filter);
            }
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

}
