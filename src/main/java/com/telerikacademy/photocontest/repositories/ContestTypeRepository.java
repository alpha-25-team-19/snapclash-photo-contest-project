package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.ContestType;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;

public interface ContestTypeRepository extends GetRepository<ContestType> {

}
