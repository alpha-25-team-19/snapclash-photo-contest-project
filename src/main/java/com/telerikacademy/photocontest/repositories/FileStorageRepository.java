package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.PhotoUrl;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;

public interface FileStorageRepository extends GetRepository<PhotoUrl> {

    void saveFilePath(PhotoUrl photoUrl);

    PhotoUrl getByPhoto(int photoId);
}
