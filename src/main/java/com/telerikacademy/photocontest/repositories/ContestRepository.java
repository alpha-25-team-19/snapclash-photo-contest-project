package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.repositories.contracts.CreateRepository;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;
import com.telerikacademy.photocontest.repositories.contracts.UpdateRepository;

import java.util.List;


public interface ContestRepository extends GetRepository<Contest>, CreateRepository<Contest>, UpdateRepository<Contest> {

    List<Contest> filter(ContestSearchParameters contestSearchParameters);

    Contest getByTitle(String contestTitle);

}
