package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.repositories.contracts.CreateRepository;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;

import java.util.List;

public interface PhotoRepository extends GetRepository<Photo>, CreateRepository<Photo> {

    List<Photo> filter(PhotoSearchParameters photoSearchParameters);

}
