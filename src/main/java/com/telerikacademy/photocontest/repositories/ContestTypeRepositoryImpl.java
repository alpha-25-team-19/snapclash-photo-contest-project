package com.telerikacademy.photocontest.repositories;


import com.telerikacademy.photocontest.models.ContestType;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContestTypeRepositoryImpl extends AbstractGenericGetRepository<ContestType> implements ContestTypeRepository {

    public ContestTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<ContestType> getAll() {
        return super.getAll(ContestType.class);
    }

    @Override
    public ContestType getById(int id) {
        return super.getByField("id", id, ContestType.class);
    }
}
