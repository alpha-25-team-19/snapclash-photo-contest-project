package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public class AbstractGenericGetRepository<T> {

    private final SessionFactory sessionFactory;

    public AbstractGenericGetRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public <V> T getByField(String fieldName, V fieldValue, Class<T> tClass) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = String.format("from %s where %s = :value", tClass.getName(), fieldName);
            Query<T> query = session.createQuery(queryString, tClass);
            query.setParameter("value", fieldValue);
            return query
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(
                            String.format("%s with %s %s not found",
                                    tClass.getSimpleName(),
                                    fieldName,
                                    fieldValue)));
        }
    }

    public List<T> getAll(Class<T> tClass) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(String.format("from %s ", tClass.getName()), tClass).getResultList();
        }
    }
}
