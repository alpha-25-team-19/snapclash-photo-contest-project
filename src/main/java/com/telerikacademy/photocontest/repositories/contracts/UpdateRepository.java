package com.telerikacademy.photocontest.repositories.contracts;

public interface UpdateRepository<T> {

    void update(T entityToUpdate);
}
