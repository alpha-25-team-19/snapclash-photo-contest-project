package com.telerikacademy.photocontest.repositories.contracts;

public interface CreateRepository<T> {

    void create(T entityToCreate);
}
