package com.telerikacademy.photocontest.repositories.contracts;

public interface DeleteRepository<T> {

    void delete(T entityToDelete);
}
