package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.PhotoReview;
import com.telerikacademy.photocontest.repositories.contracts.CreateRepository;
import com.telerikacademy.photocontest.repositories.contracts.DeleteRepository;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;

import java.util.List;

public interface PhotoReviewRepository extends GetRepository<PhotoReview>, CreateRepository<PhotoReview>, DeleteRepository<PhotoReview> {

    double getPhotoFinalScore(int photoId);

    List<PhotoReview> getByJuror(int jurorId);

    List<PhotoReview> getByPhoto(int photoId);

    int getPhotoReviewCount(int photoId);

}
