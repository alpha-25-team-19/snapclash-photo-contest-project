package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.search.UserSearchParameters;
import com.telerikacademy.photocontest.repositories.contracts.CreateRepository;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;
import com.telerikacademy.photocontest.repositories.contracts.UpdateRepository;

import java.util.List;

public interface UserRepository extends GetRepository<User>, CreateRepository<User>, UpdateRepository<User> {

    User getByUsername(String username);

    List<Ranking> filter(UserSearchParameters userSearchParameters);

    List<User> getAllEligibleJurors();

    List<User> getAllOrganisers();
}
