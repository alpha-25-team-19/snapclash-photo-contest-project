package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.PhotoUrl;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FileStorageRepositoryImpl extends AbstractGenericGetRepository<PhotoUrl> implements FileStorageRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public FileStorageRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveFilePath(PhotoUrl photoUrl) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(photoUrl);
            session.getTransaction().commit();
        }
    }

    @Override
    public PhotoUrl getByPhoto(int photoId) {
        return super.getByField("photo.id", photoId, PhotoUrl.class);
    }

    @Override
    public List<PhotoUrl> getAll() {
        return super.getAll(PhotoUrl.class);
    }

    @Override
    public PhotoUrl getById(int id) {
        return super.getByField("id", id, PhotoUrl.class);
    }
}
