package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.repositories.contracts.CreateRepository;
import com.telerikacademy.photocontest.repositories.contracts.DeleteRepository;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;
import com.telerikacademy.photocontest.repositories.contracts.UpdateRepository;

public interface RankingRepository extends GetRepository<Ranking>,
        CreateRepository<Ranking>, UpdateRepository<Ranking>, DeleteRepository<Ranking> {

    Ranking getByUser(String username);

}
