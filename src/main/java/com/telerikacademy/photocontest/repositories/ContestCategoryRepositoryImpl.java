package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.ContestCategory;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContestCategoryRepositoryImpl extends AbstractGenericGetRepository<ContestCategory> implements ContestCategoryRepository {

    public ContestCategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<ContestCategory> getAll() {
        return super.getAll(ContestCategory.class);
    }

    @Override
    public ContestCategory getById(int id) {
        return super.getByField("id", id, ContestCategory.class);
    }
}
