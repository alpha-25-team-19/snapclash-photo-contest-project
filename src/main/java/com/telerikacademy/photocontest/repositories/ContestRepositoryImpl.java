package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;
import static com.telerikacademy.photocontest.utils.QueryHelpers.like;

@Repository
public class ContestRepositoryImpl extends AbstractGenericGetRepository<Contest> implements ContestRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Contest> getAll() {
        return super.getAll(Contest.class);
    }

    @Override
    public Contest getById(int id) {
        return super.getByField("id", id, Contest.class);
    }

    @Override
    public Contest getByTitle(String contestTitle) {
        return super.getByField("title", contestTitle, Contest.class);
    }

    @Override
    public void create(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(contest);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Contest> filter(ContestSearchParameters contestSearchParameters) {
        try (Session session = sessionFactory.openSession()) {

            StringBuilder baseQuery;
            var filters = new ArrayList<String>();
            baseQuery = generateQuery(contestSearchParameters, filters);

            Query<Contest> query = session.createQuery(baseQuery.toString(), Contest.class);
            setQueryParameters(contestSearchParameters, query);

            if (query.getResultList().size() == 0) {
                throw new EntityNotFoundException(NO_RESULTS_FOUND);
            }

            return query.list();
        }
    }

    private StringBuilder generateQuery(ContestSearchParameters contestSearchParameters, ArrayList<String> filters) {
        StringBuilder baseQuery;
        if (contestSearchParameters.getUserId() != null && contestSearchParameters.getUserId() != -1) {
            baseQuery = new StringBuilder(
                    "from Contest where id in (select contest.id from Photo where photoUser.id = :id) ");
        } else {
            baseQuery = new StringBuilder(" from Contest where 1=1 ");
        }


        if (!contestSearchParameters.getContestCategory().isBlank()) {
            // check valid category? see phase switch below
            filters.add(" AND contestCategory.categoryName like lower(:contestCategory) ");
        }
        if (!contestSearchParameters.getContestType().isBlank()) {
            // check valid type? see phase switch below
            filters.add(" AND contestType.typeName like lower(:contestType) ");
        }
        if (!contestSearchParameters.getTitle().isBlank()) {
            filters.add(" AND title like lower(:title) ");
        }

        if (!contestSearchParameters.getPhase().isBlank()) {
            switch (contestSearchParameters.getPhase()) {
                case NOT_STARTED:
                    filters.add(" AND phaseOneStart > :date ");
                    break;
                case PHASE_ONE:
                    filters.add(" AND phaseOneStart < :date AND phaseTwoStart > :date ");
                    break;
                case PHASE_TWO:
                    filters.add(" AND phaseTwoStart < :date AND contestEnd > :date ");
                    break;
                case FINISHED:
                    filters.add(" AND contestEnd < :date ");
                    break;
                case NOT_FINISHED:
                    filters.add(" AND phaseOneStart < :date AND contestEnd > :date");
                    break;
                default:
                    throw new EntityNotFoundException(
                            String.format(INVALID_CONTEST_PHASE, contestSearchParameters.getPhase()));
            }
        }

        if (filters.size() > 0) {
            for (String filter : filters)
                baseQuery.append(filter);
        }
        return baseQuery;
    }

    private void setQueryParameters(ContestSearchParameters contestSearchParameters, Query<Contest> query) {
        if (contestSearchParameters.getUserId() != null && contestSearchParameters.getUserId() != -1) {
            query.setParameter("id", contestSearchParameters.getUserId());
        }
        if (!contestSearchParameters.getContestCategory().isBlank()) {
            query.setParameter("contestCategory", like(contestSearchParameters.getContestCategory()));
        }
        if (!contestSearchParameters.getContestType().isBlank()) {
            query.setParameter("contestType", like(contestSearchParameters.getContestType()));
        }
        if (!contestSearchParameters.getTitle().isBlank()) {
            query.setParameter("title", like(contestSearchParameters.getTitle()));
        }
        if (!contestSearchParameters.getPhase().isBlank()) {
            switch (contestSearchParameters.getPhase()) {
                case NOT_STARTED:
                case PHASE_ONE:
                case PHASE_TWO:
                case FINISHED:
                case NOT_FINISHED:
                    query.setParameter("date", LocalDateTime.now());
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void update(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(contest);
            session.getTransaction().commit();
        }
    }
}
