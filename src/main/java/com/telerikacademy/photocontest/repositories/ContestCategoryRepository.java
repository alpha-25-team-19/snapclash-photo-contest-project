package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.ContestCategory;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;

public interface ContestCategoryRepository extends GetRepository<ContestCategory> {

}
