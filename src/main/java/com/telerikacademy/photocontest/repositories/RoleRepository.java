package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Role;
import com.telerikacademy.photocontest.repositories.contracts.GetRepository;

public interface RoleRepository extends GetRepository<Role> {

    Role getByName(String roleName);
}
