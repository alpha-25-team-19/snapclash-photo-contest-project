package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RankingRepositoryImpl extends AbstractGenericGetRepository<Ranking> implements RankingRepository {

    private final SessionFactory sessionFactory;

    public RankingRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Ranking> getAll() {
        return super.getAll(Ranking.class);
    }

    @Override
    public Ranking getById(int id) {
        return super.getByField("id", id, Ranking.class);
    }

    @Override
    public void create(Ranking ranking) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(ranking);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Ranking ranking) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(ranking);
            session.getTransaction().commit();
        }
    }

    @Override
    public Ranking getByUser(String username) {
        return super.getByField("user.username", username, Ranking.class);
    }

    @Override
    public void delete(Ranking ranking) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(ranking);
            session.getTransaction().commit();
        }
    }
}
