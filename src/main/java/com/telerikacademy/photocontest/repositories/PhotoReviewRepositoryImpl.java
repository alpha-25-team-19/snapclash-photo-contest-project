package com.telerikacademy.photocontest.repositories;


import com.telerikacademy.photocontest.models.PhotoReview;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhotoReviewRepositoryImpl extends AbstractGenericGetRepository<PhotoReview> implements PhotoReviewRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoReviewRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PhotoReview> getAll() {
        return super.getAll(PhotoReview.class);
    }

    @Override
    public PhotoReview getById(int id) {
        return super.getByField("id", id, PhotoReview.class);
    }


    @Override
    public void create(PhotoReview photoReview) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(photoReview);
            session.getTransaction().commit();
        }
    }

    @Override
    public double getPhotoFinalScore(int photoId) {
        try (Session session = sessionFactory.openSession()) {
            String query = "select avg(photoScore) from PhotoReview where photo.id = :photoId";

            Query avgQuery = session.createQuery(query);
            avgQuery.setParameter("photoId", photoId);
            return (Double) avgQuery.getSingleResult();
        }
    }

    @Override
    public List<PhotoReview> getByJuror(int jurorId) {
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoReview> query = session.createQuery(
                    "from PhotoReview where juror.id = :jurorId", PhotoReview.class);
            query.setParameter("jurorId", jurorId);

            return query.getResultList();
        }
    }

    @Override
    public List<PhotoReview> getByPhoto(int photoId) {
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoReview> query = session.createQuery("from PhotoReview where photo.id = :photoId", PhotoReview.class);
            query.setParameter("photoId", photoId);

            return query.getResultList();
        }
    }

    @Override
    public int getPhotoReviewCount(int photoId) {
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoReview> query = session.createQuery("from PhotoReview where photo.id = :photoId", PhotoReview.class);
            query.setParameter("photoId", photoId);

            return query.getResultList().size();
        }
    }

    @Override
    public void delete(PhotoReview photoReview) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(photoReview);
            session.getTransaction().commit();
        }
    }
}
