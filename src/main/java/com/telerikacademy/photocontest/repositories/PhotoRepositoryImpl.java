package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.repositories.contracts.AbstractGenericGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.NO_RESULTS_FOUND;
import static com.telerikacademy.photocontest.utils.QueryHelpers.like;

@Repository
public class PhotoRepositoryImpl extends AbstractGenericGetRepository<Photo> implements PhotoRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(photo);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Photo> getAll() {
        return super.getAll(Photo.class);
    }

    @Override
    public Photo getById(int id) {
        return super.getByField("id", id, Photo.class);
    }

    @Override
    public List<Photo> filter(PhotoSearchParameters photoSearchParameters) {
        try (Session session = sessionFactory.openSession()) {

            StringBuilder baseQuery = new StringBuilder("from Photo where 1=1 ");
            var filters = new ArrayList<String>();
            generateQuery(photoSearchParameters, baseQuery, filters);

            Query<Photo> query = session.createQuery(baseQuery.toString(), Photo.class);
            setQueryParameters(photoSearchParameters, query);

            if (query.getResultList().size() == 0) {
                throw new EntityNotFoundException(NO_RESULTS_FOUND);
            }

            return query.getResultList();

        }
    }

    private void setQueryParameters(PhotoSearchParameters photoSearchParameters, Query<Photo> query) {
        if (!photoSearchParameters.getContest().isBlank()) {
            query.setParameter("contest", like(photoSearchParameters.getContest()));
        }

        if (!photoSearchParameters.getUsername().isBlank()) {
            query.setParameter("username", like(photoSearchParameters.getUsername()));
        }
    }

    private void generateQuery(PhotoSearchParameters photoSearchParameters, StringBuilder baseQuery, ArrayList<String> filters) {
        if (!photoSearchParameters.getContest().isBlank()) {
            filters.add("AND contest.title like lower(:contest)");
        }

        if (!photoSearchParameters.getUsername().isBlank()) {
            filters.add("AND photoUser.username like lower(:username)");
        }

        if (filters.size() > 0) {
            for (String filter : filters) {
                baseQuery.append(filter);
            }
        }
    }
}
