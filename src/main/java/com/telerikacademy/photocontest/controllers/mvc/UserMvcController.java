package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.UserModelMapper;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.models.transfer.UpdateUserDto;
import com.telerikacademy.photocontest.repositories.ContestRepository;
import com.telerikacademy.photocontest.services.ContestService;
import com.telerikacademy.photocontest.services.RankingService;
import com.telerikacademy.photocontest.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@Controller
@RequestMapping("users")
public class UserMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final RankingService rankingService;
    private final UserService userService;
    private final UserModelMapper userModelMapper;
    private final ContestService contestService;
    private final ContestRepository contestRepository;

    @Autowired
    public UserMvcController(AuthenticationHelper authenticationHelper,
                             RankingService rankingService,
                             UserService userService,
                             UserModelMapper userModelMapper,
                             ContestService contestService,
                             ContestRepository contestRepository) {
        this.authenticationHelper = authenticationHelper;
        this.rankingService = rankingService;
        this.userService = userService;
        this.userModelMapper = userModelMapper;
        this.contestService = contestService;
        this.contestRepository = contestRepository;
    }

    @ModelAttribute("activeContests")
    public List<Contest> populateActiveContests() {
        ContestSearchParameters contestSearchParameters =
                new ContestSearchParameters(NOT_FINISHED, "", "", "", -1);

        try {
            return contestRepository.filter(contestSearchParameters);
        } catch (EntityNotFoundException exc) {
            return null;
        }
    }


    @GetMapping("{id}")
    public String showUserDashboard(@PathVariable int id, HttpSession session, Model model) {
        User currentUser = getAuthorizedUser(model, session, id);
        if (currentUser == null) return "errors/403";

        model.addAttribute("currentUser", currentUser);

        Ranking currentUserRanking;

        try {
            currentUserRanking = rankingService.getByUser(currentUser, currentUser);
            model.addAttribute("userRanking", currentUserRanking);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        }

        model.addAttribute("currentUserRanking", currentUserRanking);

        List<Contest> currentContests;

        try {
            ContestSearchParameters cspPhaseOne =
                    new ContestSearchParameters(NOT_FINISHED, "", "", "", currentUser.getId());
            currentContests = contestService.filter(cspPhaseOne, currentUser);
            model.addAttribute("userCurrentContests", currentContests);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        } catch (EntityNotFoundException ignored) {
        }

        List<Contest> finishedContests;

        try {
            ContestSearchParameters cspPhaseOne =
                    new ContestSearchParameters(FINISHED, "", "", "", currentUser.getId());
            finishedContests = contestService.filter(cspPhaseOne, currentUser);
            model.addAttribute("userFinishedContests", finishedContests);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        } catch (EntityNotFoundException ignored) {
        }

        return "user-dashboard";
    }

    @GetMapping("{id}/update")
    public String showUpdateUserPage(@PathVariable int id, HttpSession session, Model model) {
        User currentUser = getAuthorizedUser(model, session, id);
        if (currentUser == null) return "errors/403";
        model.addAttribute("currentUser", currentUser);

        User userToUpdate;

        try {
            userToUpdate = userService.getById(id, currentUser);
            model.addAttribute("userToUpdate", userToUpdate);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        }

        model.addAttribute("updateUserDto", userModelMapper.fromUser(userToUpdate));
        return "user-profile";
    }

    @PostMapping("{id}/update")
    public String handleUpdateUser(Model model,
                                   HttpSession session,
                                   @PathVariable int id,
                                   @Valid @ModelAttribute("updateUserDto") UpdateUserDto updateUserDto,
                                   BindingResult bindingResult) {
        User currentUser = getAuthorizedUser(model, session, id);
        if (currentUser == null) return "errors/403";

        if (bindingResult.hasErrors()) {
            return "user-profile";
        }

        try {
            User userToUpdate = userModelMapper.userFromDto(updateUserDto, id);
            userService.update(userToUpdate, currentUser);
        } catch (EntityNotFoundException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        }

        return "redirect:/";
    }

    private User getAuthorizedUser(Model model, HttpSession session, int id) {
        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            if (!currentUser.getRole().getRoleName().equals(ORGANISER_ROLE) && currentUser.getId() != id) {
                return null;
            }
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return null;
        }
        return currentUser;
    }

}
