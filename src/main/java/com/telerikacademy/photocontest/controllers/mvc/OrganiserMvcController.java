package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.ContestModelMapper;
import com.telerikacademy.photocontest.controllers.mappers.LeaderboardModelMapper;
import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.InvalidContestPhaseException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.models.search.UserSearchParameters;
import com.telerikacademy.photocontest.models.transfer.ContestDto;
import com.telerikacademy.photocontest.services.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.photocontest.utils.GlobalConstants.ORGANISER_ROLE;


@Controller
@RequestMapping("organisers")
public class OrganiserMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final RankingService rankingService;
    private final UserService userService;
    private final ContestService contestService;
    private final PhotoService photoService;
    private final ContestCategoryService contestCategoryService;
    private final ContestTypeService contestTypeService;
    private final ContestModelMapper contestModelMapper;
    private final LeaderboardModelMapper leaderboardModelMapper;

    public OrganiserMvcController(AuthenticationHelper authenticationHelper,
                                  RankingService rankingService,
                                  UserService userService,
                                  ContestService contestService,
                                  PhotoService photoService,
                                  ContestCategoryService contestCategoryService,
                                  ContestTypeService contestTypeService,
                                  ContestModelMapper contestModelMapper,
                                  LeaderboardModelMapper leaderboardModelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.rankingService = rankingService;
        this.userService = userService;
        this.contestService = contestService;
        this.photoService = photoService;
        this.contestCategoryService = contestCategoryService;
        this.contestTypeService = contestTypeService;
        this.contestModelMapper = contestModelMapper;
        this.leaderboardModelMapper = leaderboardModelMapper;
    }

    @ModelAttribute("rankings")
    public List<Ranking> populateRanking(HttpSession httpSession) {

        User currentUser = getCurrentUser(httpSession);
        if (currentUser == null) return null;

        return rankingService.getAll(currentUser);
    }


    @ModelAttribute("users")
    public List<User> populateUsers(HttpSession httpSession, Model model) {
        User currentUser = getCurrentUser(httpSession);
        if (currentUser == null) return null;

        model.addAttribute("currentUser", currentUser);
        return userService.getAll(currentUser);
    }

    @ModelAttribute("photos")
    public List<Photo> populatePhotos() {
        return photoService.getAll();
    }

    @ModelAttribute("jurors")
    public List<User> populateJurors() {
        return userService.getAllEligibleJurors();
    }

    @ModelAttribute("contests")
    public List<Contest> populateContests() {
        return contestService.getAll();
    }

    @ModelAttribute("contestTypes")
    public List<ContestType> populateContestTypes() {
        return contestTypeService.getAll();
    }

    @ModelAttribute("contestCategories")
    public List<ContestCategory> populateContestCategories() {
        return contestCategoryService.getAll();
    }

    @GetMapping("/photos")
    public String showAllPhotos(Model model, HttpSession httpSession) {
        User currentUser = authenticationHelper.tryGetUser(httpSession);
        model.addAttribute("currentUser", currentUser);
        return "photos";
    }

    @GetMapping("/contests")
    public String showAllContests(Model model, HttpSession httpSession) {
        if (currentUserNotAuthorised(model, httpSession)) return "errors/403";
        return "contests";
    }

    @GetMapping()
    public String showAllUsers(Model model, HttpSession httpSession) {
        if (currentUserNotAuthorised(model, httpSession)) return "errors/403";
        return "organiser";
    }

    @GetMapping("/filter")
    public String handleGetUserSearch() {
        return "redirect:/organisers";
    }

    @PostMapping("/filter")
    public String handleUserSearch(Model model,
                                   HttpSession session,
                                   @RequestParam(required = false) Optional<String> name,
                                   @RequestParam(required = false) Optional<String> ranking,
                                   @ModelAttribute("userSearchParameters") UserSearchParameters userSearchParameters) {

        User currentUser = authenticationHelper.tryGetUser(session);
        List<Ranking> result;

        try {
            userSearchParameters.setName(name.orElse(""));
            userSearchParameters.setRanking(ranking.orElse(""));
            result = userService.filter(userSearchParameters, currentUser);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/404";
        }

        model.addAttribute("currentUser", currentUser);
        model.addAttribute("userSearchParameters", userSearchParameters);
        model.addAttribute("rankings", result);

        return "organiser";
    }

    @GetMapping("/contests/filter")
    public String handleGetContestSearch() {
        return "redirect:/contests";
    }

    @PostMapping("/contests/filter")
    public String handleContestSearch(Model model,
                                      HttpSession session,
                                      @RequestParam(required = false) Optional<String> phase,
                                      @RequestParam(required = false) Optional<String> contestCategory,
                                      @RequestParam(required = false) Optional<String> contestType,
                                      @RequestParam(required = false) Optional<String> title,
                                      @RequestParam(required = false) Optional<Integer> userId,
                                      @ModelAttribute("contestSearchParameters") ContestSearchParameters contestSearchParameters) {

        User currentUser = authenticationHelper.tryGetUser(session);
        List<Contest> result;

        try {
            contestSearchParameters.setPhase(phase.orElse(""));
            contestSearchParameters.setContestCategory(contestCategory.orElse(""));
            contestSearchParameters.setContestType(contestType.orElse(""));
            contestSearchParameters.setTitle(title.orElse(""));
            contestSearchParameters.setUserId(userId.orElse(-1));
            result = contestService.filter(contestSearchParameters, currentUser);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/404";
        }

        model.addAttribute("currentUser", currentUser);
        model.addAttribute("contestSearchParameters", contestSearchParameters);
        model.addAttribute("contests", result);

        return "contests";
    }

    @GetMapping("/photos/filter")
    public String handleGetPhotoSearch() {
        return "redirect:/photos";
    }

    @PostMapping("/photos/filter")
    public String handlePhotoSearch(Model model,
                                    HttpSession session,
                                    @RequestParam(required = false) Optional<String> contest,
                                    @RequestParam(required = false) Optional<String> username,
                                    @ModelAttribute("photoSearchParameters") PhotoSearchParameters photoSearchParameters) {

        User currentUser = authenticationHelper.tryGetUser(session);
        List<Photo> result;

        try {
            photoSearchParameters.setContest(contest.orElse(""));
            photoSearchParameters.setUsername(username.orElse(""));
            result = photoService.filter(photoSearchParameters);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/404";
        }

        model.addAttribute("currentUser", currentUser);
        model.addAttribute("photoSearchParameters", photoSearchParameters);
        model.addAttribute("photos", result);

        return "photos";
    }

    @GetMapping("/contests/create")
    public String showCreateContest(Model model, HttpSession session) {

        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        }

        model.addAttribute("currentUser", currentUser);
        model.addAttribute("contestDto", new ContestDto());
        return "contest-create";
    }

    @PostMapping("/contests/create")
    public String handleCreateContest(@Valid @ModelAttribute("contestDto") ContestDto contestDto,
                                      BindingResult bindingResult, Model model,
                                      HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "contest-create";
        }

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            Contest contest = contestModelMapper.fromDto(contestDto);
            contestService.createContest(currentUser, contest);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        } catch (DuplicateEntityException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        }

        return "redirect:/contests";
    }

    @GetMapping("/contests/{id}/leaderboard")
    public String handleAllocateLeaderboardPoints(HttpSession session, Model model, @PathVariable int id) {
        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            ContestLeaderboard contestLeaderboard = leaderboardModelMapper.fromContestId(id);
            model.addAttribute("currentUser", currentUser);
            contestService.allocateContestPoints(contestLeaderboard, currentUser);
        } catch (UnauthorizedOperationException | InvalidContestPhaseException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        } catch (DuplicateEntityException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        }

        return "redirect:/organisers/contests";
    }

    private User getCurrentUser(HttpSession httpSession) {
        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(httpSession);
        } catch (UnauthorizedOperationException exc) {
            return null;
        }
        return currentUser;
    }

    private boolean currentUserNotAuthorised(Model model, HttpSession httpSession) {
        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(httpSession);
            if (!currentUser.getRole().getRoleName().equalsIgnoreCase(ORGANISER_ROLE)) {
                return true;
            }
        } catch (UnauthorizedOperationException exc) {
            return true;
        }

        model.addAttribute("currentUser", currentUser);
        return false;
    }

}
