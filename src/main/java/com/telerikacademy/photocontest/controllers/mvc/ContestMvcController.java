package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.LeaderboardModelMapper;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.InvalidContestPhaseException;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.repositories.PhotoRepository;
import com.telerikacademy.photocontest.services.ContestService;
import com.telerikacademy.photocontest.services.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.photocontest.utils.MvcHelpers.setCurrentUser;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final LeaderboardModelMapper leaderboardModelMapper;
    private final PhotoRepository photoRepository;
    private final FileStorageService fileStorageService;

    @Autowired
    public ContestMvcController(AuthenticationHelper authenticationHelper,
                                ContestService contestService,
                                LeaderboardModelMapper leaderboardModelMapper,
                                PhotoRepository photoRepository,
                                FileStorageService fileStorageService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.leaderboardModelMapper = leaderboardModelMapper;
        this.photoRepository = photoRepository;
        this.fileStorageService = fileStorageService;
    }

    @ModelAttribute("allContests")
    public List<Contest> populateContests() {
        return contestService.getAll();
    }

    @GetMapping
    public String showAllContests(Model model, HttpSession session) {
        setCurrentUser(authenticationHelper, model, session);

        return "contests-public";
    }

    @GetMapping("{id}")
    public String showContest(@PathVariable int id, Model model, HttpSession session) {
        setCurrentUser(authenticationHelper, model, session);
        ContestLeaderboard contestLeaderboard;
        Contest contest;

        try {
            contest = contestService.getById(id);
            model.addAttribute("contest", contest);

            List<Photo> contestPhotos;

            try {
                contestPhotos = photoRepository.filter(new PhotoSearchParameters(contest.getTitle(), ""));
                List<PhotoUrl> contestPhotoUrls = new ArrayList<>();

                for (Photo photo : contestPhotos) {
                    contestPhotoUrls.add(fileStorageService.getPhotoUrlById(photo.getId()));
                }
                model.addAttribute("contestPhotos", contestPhotoUrls);
            } catch (EntityNotFoundException exc) {
                model.addAttribute("contestPhotos", null);
            }

            contestLeaderboard = leaderboardModelMapper.fromContestId(id);
            model.addAttribute("leaderboard", contestLeaderboard);
        } catch (EntityNotFoundException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        } catch (InvalidContestPhaseException ignored) {
        }

        return "contest";
    }
}
