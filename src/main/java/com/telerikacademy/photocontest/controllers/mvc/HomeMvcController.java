package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.repositories.ContestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.NOT_FINISHED;
import static com.telerikacademy.photocontest.utils.MvcHelpers.setCurrentUser;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ContestRepository contestRepository;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper, ContestRepository contestRepository) {
        this.authenticationHelper = authenticationHelper;
        this.contestRepository = contestRepository;
    }

    @ModelAttribute("activeContests")
    public List<Contest> populateActiveContests() {
        ContestSearchParameters contestSearchParameters =
                new ContestSearchParameters(NOT_FINISHED, "", "", "", -1);

        try {
            return contestRepository.filter(contestSearchParameters);
        } catch (EntityNotFoundException exc) {
            return null;
        }
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        setCurrentUser(authenticationHelper, model, session);

        return "index";
    }

}
