package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.UserModelMapper;
import com.telerikacademy.photocontest.exceptions.AuthenticationFailureException;
import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.transfer.LoginDto;
import com.telerikacademy.photocontest.models.transfer.RegisterDto;
import com.telerikacademy.photocontest.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.telerikacademy.photocontest.utils.GlobalConstants.ORGANISER_ROLE;

@Controller
@RequestMapping
public class AuthenticationMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserModelMapper userModelMapper;

    @Autowired
    public AuthenticationMvcController(AuthenticationHelper authenticationHelper,
                                       UserService userService,
                                       UserModelMapper userModelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDto", new LoginDto());

        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto") LoginDto loginDto,
                              BindingResult bindingResult,
                              HttpSession httpSession) {

        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(loginDto.getUsername(), loginDto.getPassword());
            httpSession.setAttribute("currentUserUsername", loginDto.getUsername());
        } catch (AuthenticationFailureException exc) {
            bindingResult.rejectValue("username", "auth_error", exc.getMessage());
            return "login";
        }

        User user = userService.getByUsername(loginDto.getUsername());
        if (user.getRole().getRoleName().equals(ORGANISER_ROLE)) {
            return "redirect:/organisers";
        } else {
            return "redirect:/users/" + user.getId();
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUserUsername");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("registerDto", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("registerDto") RegisterDto registerDto,
                                 BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())) {
            bindingResult.rejectValue("password", "password_error", "Passwords don't match!");
            return "register";
        }

        try {
            User userToRegister = userModelMapper.userFromDto(registerDto);
            Ranking ranking = userModelMapper.generateUserRanking(userToRegister);
            userService.createUser(userToRegister, ranking);
        } catch (DuplicateEntityException exc) {
            bindingResult.rejectValue("username", "username_error", exc.getMessage());
            return "register";
        }

        return "redirect:/login";
    }
}
