package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.PhotoModelMapper;
import com.telerikacademy.photocontest.controllers.mappers.PhotoReviewMapper;
import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.FileStorageException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.models.transfer.PhotoDto;
import com.telerikacademy.photocontest.models.transfer.PhotoReviewDto;
import com.telerikacademy.photocontest.repositories.ContestRepository;
import com.telerikacademy.photocontest.services.FileStorageService;
import com.telerikacademy.photocontest.services.PhotoReviewService;
import com.telerikacademy.photocontest.services.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;
import static com.telerikacademy.photocontest.utils.MvcHelpers.setCurrentUser;

@Controller
@RequestMapping("/photos")
public class PhotoMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final PhotoModelMapper photoModelMapper;
    private final PhotoService photoService;
    private final ContestRepository contestRepository;
    private final FileStorageService fileStorageService;
    private final PhotoReviewService photoReviewService;
    private final PhotoReviewMapper photoReviewMapper;

    @Autowired
    public PhotoMvcController(AuthenticationHelper authenticationHelper,
                              PhotoModelMapper photoModelMapper,
                              PhotoService photoService,
                              ContestRepository contestRepository,
                              FileStorageService fileStorageService,
                              PhotoReviewService photoReviewService,
                              PhotoReviewMapper photoReviewMapper) {
        this.authenticationHelper = authenticationHelper;
        this.photoModelMapper = photoModelMapper;
        this.photoService = photoService;
        this.contestRepository = contestRepository;
        this.fileStorageService = fileStorageService;
        this.photoReviewService = photoReviewService;
        this.photoReviewMapper = photoReviewMapper;
    }

    @ModelAttribute("contests")
    public List<Contest> populateContests() {
        ContestSearchParameters contestSearchParameters =
                new ContestSearchParameters(PHASE_ONE, "", "", "", -1);

        return contestRepository.filter(contestSearchParameters);
    }

    @GetMapping()
    public String showAllPhotos(Model model, HttpSession session) {
        setCurrentUser(authenticationHelper, model, session);
        model.addAttribute("allPhotos", fileStorageService.getAllPhotoUrls());

        return "photos-public";
    }

    @GetMapping("/search")
    public String handleGetPhotoSearch() {
        return "redirect:/photos";
    }

    @PostMapping("/search")
    public String handlePhotoSearch(Model model,
                                    HttpSession session,
                                    @RequestParam(required = false) Optional<String> contest,
                                    @RequestParam(required = false) Optional<String> username,
                                    @ModelAttribute("photoSearchParameters") PhotoSearchParameters photoSearchParameters) {

        setCurrentUser(authenticationHelper, model, session);
        List<Photo> result;
        List<PhotoUrl> searchResult = new ArrayList<>();

        try {
            photoSearchParameters.setContest(contest.orElse(""));
            photoSearchParameters.setUsername(username.orElse(""));
            result = photoService.filter(photoSearchParameters);

            for (Photo photo : result) {
                searchResult.add(fileStorageService.getPhotoUrlById(photo.getId()));
            }

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/404";
        }

        model.addAttribute("photoSearchParameters", photoSearchParameters);
        model.addAttribute("allPhotos", searchResult);

        return "photos-public";
    }

    @GetMapping("{id}")
    public String showSinglePhoto(@PathVariable int id, Model model, HttpSession session) {
        setCurrentUser(authenticationHelper, model, session);

        try {
            PhotoUrl photoUrl = fileStorageService.getPhotoUrlById(id);
            model.addAttribute("photo", photoUrl);

            if (photoUrl.getPhoto().getContest().isAwarded()) {
                double photoFinalScore = photoReviewService.getPhotoFinalScore(photoUrl.getPhoto());
                model.addAttribute("photoScore", photoFinalScore);
            }

        } catch (EntityNotFoundException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        }

        return "photo";
    }

    @GetMapping("/new")
    public String showCreatePhoto(Model model, HttpSession session) {

        User currentUser = getCurrentUser(session, model);
        if (currentUser == null) return "errors/403";

        model.addAttribute("currentUser", currentUser);

        ContestSearchParameters contestSearchParameters =
                new ContestSearchParameters(PHASE_ONE, "", "", "", -1);

        try {
            contestRepository.filter(contestSearchParameters);
        } catch (EntityNotFoundException exc) {
            return "errors/404";
        }

        model.addAttribute("photoDto", new PhotoDto());
        return "photo-new";
    }

    @PostMapping("/new")
    public String handleCreatePhoto(@Valid @ModelAttribute("photoDto") PhotoDto photoDto,
                                    BindingResult bindingResult, Model model,
                                    HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "photo-new";
        }

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            photoDto.setUserId(currentUser.getId());
            Photo photo = photoModelMapper.fromDto(photoDto);
            photoService.createPhoto(currentUser, photo);
            return "redirect:/photos/" + photo.getId() + "/upload";
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        } catch (EntityNotFoundException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        }
    }

    @GetMapping("{id}/upload")
    public String showUploadPhoto(@PathVariable int id, HttpSession session, Model model) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        }

        model.addAttribute("photoId", id);
        return "photo-upload";
    }

    @PostMapping("{id}/upload")
    public String handlePhotoUpload(@PathVariable int id,
                                    HttpSession session,
                                    Model model,
                                    @RequestParam("file") MultipartFile file,
                                    RedirectAttributes attributes) {

        User currentUser = getCurrentUser(session, model);
        if (currentUser == null) return "errors/403";

        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "redirect:/photos/" + id + "/upload";
        }

        Photo photo = photoService.getById(id);

        try {
            fileStorageService.storeFile(file, photo, currentUser);
        } catch (UnauthorizedOperationException | DuplicateEntityException | FileStorageException exc) {
            model.addAttribute("error", exc.getMessage());
            return "redirect:/photos/" + id + "/upload";
        }

        attributes.addFlashAttribute("message", "You successfully uploaded your photo!");

        return "redirect:/photos/" + id + "/upload";

    }

    @GetMapping("{id}/review/new")
    public String showCreatePhotoReview(@PathVariable int id, Model model, HttpSession session) {
        PhotoUrl photoUrl;

        try {
            photoUrl = fileStorageService.getPhotoUrlById(id);
            model.addAttribute("photo", photoUrl);
        } catch (EntityNotFoundException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        }

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        }

        model.addAttribute("photoReviewDto", new PhotoReviewDto());
        return "photo-review-new";
    }

    @PostMapping("{id}/review/new")
    public String handleCreatePhotoReview(@PathVariable int id,
                                          @ModelAttribute("photoReviewDto") PhotoReviewDto photoReviewDto,
                                          Model model,
                                          HttpSession session,
                                          BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "photo-review-new";
        }

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        }


        try {
            photoReviewDto.setJurorId(currentUser.getId());
            photoReviewDto.setPhotoId(id);
            PhotoReview photoReview = photoReviewMapper.fromDto(photoReviewDto);
            photoReviewService.createPhotoReview(photoReview, currentUser);
        } catch (DuplicateEntityException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        }

        return "redirect:/photos/" + id;

    }

    @GetMapping("{id}/reviews")
    public String showPhotoReviews(@PathVariable int id, Model model, HttpSession session) {
        setCurrentUser(authenticationHelper, model, session);

        PhotoUrl photoUrl;

        try {
            photoUrl = fileStorageService.getPhotoUrlById(id);
            model.addAttribute("photo", photoUrl);

            if (photoUrl.getPhoto().getContest().isAwarded()) {
                double photoFinalScore = photoReviewService.getPhotoFinalScore(photoUrl.getPhoto());
                model.addAttribute("photoScore", photoFinalScore);
            }

        } catch (EntityNotFoundException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        }

        model.addAttribute("photoReviews", photoReviewService.getByPhoto(id));

        return "photo-reviews";
    }

    private User getCurrentUser(HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return null;
        }
        return currentUser;
    }

}
