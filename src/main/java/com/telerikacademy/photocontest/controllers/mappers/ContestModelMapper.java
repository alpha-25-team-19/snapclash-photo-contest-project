package com.telerikacademy.photocontest.controllers.mappers;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.transfer.ContestDto;
import com.telerikacademy.photocontest.repositories.ContestCategoryRepository;
import com.telerikacademy.photocontest.repositories.ContestTypeRepository;
import com.telerikacademy.photocontest.repositories.UserRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Component
public class ContestModelMapper {

    private final ContestTypeRepository contestTypeRepository;
    private final ContestCategoryRepository contestCategoryRepository;
    private final UserRepository userRepository;

    public ContestModelMapper(ContestTypeRepository contestTypeRepository,
                              ContestCategoryRepository contestCategoryRepository,
                              UserRepository userRepository) {
        this.contestTypeRepository = contestTypeRepository;
        this.contestCategoryRepository = contestCategoryRepository;
        this.userRepository = userRepository;
    }


    public Contest fromDto(ContestDto contestDto) {
        Contest contest = new Contest();
        dtoToObject(contest, contestDto);

        return contest;
    }

    private void dtoToObject(Contest contest, ContestDto contestDto) {
        contest.setTitle(contestDto.getTitle());
        contest.setContestCategory(contestCategoryRepository.getById(contestDto.getContestCategoryId()));
        contest.setPhaseOneStart(contestDto.getPhaseOneStart());
        contest.setPhaseTwoStart(contestDto.getPhaseTwoStart());
        contest.setContestEnd(contestDto.getContestEnd());
        checkContestInvitations(contest, contestDto);
        Set<User> jury = populateContestUserList(contestDto.getJuryIds());
        contest.setJury(jury);
    }

    private void checkContestInvitations(Contest contest, ContestDto contestDto) {
        if (contestDto.getInvitedUsersIds() != null && contestDto.getInvitedUsersIds().size() > 0) {
            contest.setContestType(contestTypeRepository.getById(2));
            Set<User> invitedUsers = populateContestUserList(contestDto.getInvitedUsersIds());
            contest.setInvitedUsers(invitedUsers);
        } else {
            contest.setInvitedUsers(new HashSet<>());
            contest.setContestType(contestTypeRepository.getById(1));
        }
    }

    private Set<User> populateContestUserList(ArrayList<Integer> userIds) {
        Set<User> contestList = new HashSet<>();
        User contestMember;

        for (Integer userId : userIds) {
            if (userId == null) {
                continue;
            }
            contestMember = userRepository.getById(userId);
            contestList.add(contestMember);
        }
        return contestList;
    }
}
