package com.telerikacademy.photocontest.controllers.mappers;

import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestLeaderboard;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.repositories.ContestRepository;
import com.telerikacademy.photocontest.repositories.PhotoRepository;
import com.telerikacademy.photocontest.services.PhotoReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

import static java.util.stream.Collectors.toMap;

@Component
public class LeaderboardModelMapper {

    private final PhotoRepository photoRepository;
    private final PhotoReviewService photoReviewService;
    private final ContestRepository contestRepository;

    @Autowired
    public LeaderboardModelMapper(PhotoRepository photoRepository,
                                  PhotoReviewService photoReviewService,
                                  ContestRepository contestRepository) {
        this.photoRepository = photoRepository;
        this.photoReviewService = photoReviewService;
        this.contestRepository = contestRepository;
    }

    public ContestLeaderboard fromContestId(int contestId) {
        LinkedHashMap<String, Double> photoScores = getAllFinalPhotoScores(contestId);

        HashMap<String, Double> firstPlaceWinners = new HashMap<>();
        HashMap<String, Double> secondPlaceWinners = new HashMap<>();
        HashMap<String, Double> thirdPlaceWinners = new HashMap<>();

        if (!photoScores.isEmpty()) {
            fillAllWinnerMaps(photoScores, firstPlaceWinners, secondPlaceWinners, thirdPlaceWinners);
        }

        ContestLeaderboard contestLeaderboard = new ContestLeaderboard();

        contestLeaderboard.setFirstPlaceWinners(firstPlaceWinners);
        contestLeaderboard.setSecondPlaceWinners(secondPlaceWinners);
        contestLeaderboard.setThirdPlaceWinners(thirdPlaceWinners);

        contestLeaderboard.setContest(contestRepository.getById(contestId));

        return contestLeaderboard;
    }

    private LinkedHashMap<String, Double> getAllFinalPhotoScores(int contestId) {
        Contest contest = contestRepository.getById(contestId);
        List<Photo> contestPhotos = new ArrayList<>();

        try {
             contestPhotos = photoRepository.filter(new PhotoSearchParameters(contest.getTitle(), ""));
        } catch (EntityNotFoundException ignored) {
        }


        HashMap<String, Double> photoScores = new HashMap<>();

        for (Photo photo : contestPhotos) {
            photoScores.put(photo.getPhotoUser().getUsername(), photoReviewService.getPhotoFinalScore(photo));
        }

        return photoScores.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    }

    private void fillAllWinnerMaps(LinkedHashMap<String, Double> photoScores,
                                   HashMap<String, Double> firstPlaceWinners,
                                   HashMap<String, Double> secondPlaceWinners,
                                   HashMap<String, Double> thirdPlaceWinners) {

        String winner = photoScores.entrySet().iterator().next().getKey();
        double tempScore = photoScores.entrySet().iterator().next().getValue();

        photoScores.entrySet().remove(photoScores.entrySet().iterator().next());

        firstPlaceWinners.put(winner, tempScore);

        fillWinnerMap(photoScores, firstPlaceWinners, secondPlaceWinners, tempScore);

        if (photoScores.isEmpty()) {
            return;
        } else {
            tempScore = photoScores.entrySet().iterator().next().getValue();
            photoScores.entrySet().remove(photoScores.entrySet().iterator().next());
        }

        fillWinnerMap(photoScores, secondPlaceWinners, thirdPlaceWinners, tempScore);

        if (photoScores.isEmpty()) {
            return;
        } else {
            tempScore = photoScores.entrySet().iterator().next().getValue();
            photoScores.entrySet().remove(photoScores.entrySet().iterator().next());
        }

        fillFinalWinnerMap(photoScores, thirdPlaceWinners, tempScore);
    }

    private void fillWinnerMap(LinkedHashMap<String, Double> photoScores,
                               HashMap<String, Double> winnersMap,
                               HashMap<String, Double> nextWinnersMap,
                               double tempScore) {

        while (!photoScores.isEmpty()) {
            String currentPhotoUser = photoScores.entrySet().iterator().next().getKey();
            double currentScore = photoScores.entrySet().iterator().next().getValue();

            if (tempScore == currentScore) {
                winnersMap.put(currentPhotoUser, currentScore);
                photoScores.entrySet().remove(photoScores.entrySet().iterator().next());
            } else {
                nextWinnersMap.put(currentPhotoUser, currentScore);
                break;
            }
        }
    }

    private void fillFinalWinnerMap(LinkedHashMap<String, Double> photoScores,
                                    HashMap<String, Double> lastWinnersMap,
                                    double tempScore) {
        while (!photoScores.isEmpty()) {
            String currentPhotoUser = photoScores.entrySet().iterator().next().getKey();
            double currentScore = photoScores.entrySet().iterator().next().getValue();
            photoScores.entrySet().remove(photoScores.entrySet().iterator().next());

            if (tempScore == currentScore) {
                lastWinnersMap.put(currentPhotoUser, currentScore);
            } else {
                break;
            }
        }
    }
}
