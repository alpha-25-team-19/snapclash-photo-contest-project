package com.telerikacademy.photocontest.controllers.mappers;

import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.Role;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.transfer.*;
import com.telerikacademy.photocontest.repositories.RankingRepository;
import com.telerikacademy.photocontest.repositories.RoleRepository;
import com.telerikacademy.photocontest.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@Component
public class UserModelMapper {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final RankingRepository rankingRepository;

    @Autowired
    public UserModelMapper(UserRepository userRepository,
                           RoleRepository roleRepository,
                           RankingRepository rankingRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.rankingRepository = rankingRepository;
    }


    public User userFromDto(UserDto userDto) {
        User user = new User();
        userDtoToObject(userDto, user);
        return user;
    }

    public User userFromDto(UpdateUserDto updateUserDto, int id) {
        User user = userRepository.getById(id);
        userDtoToObject(updateUserDto, user);
        return user;
    }

    public UpdateUserDto fromUser(User user) {
        UpdateUserDto updateUserDto = new UpdateUserDto();

        updateUserDto.setFirstName(user.getFirstName());
        updateUserDto.setLastName(user.getLastName());
        updateUserDto.setPromotedToOrganiser(false);

        return updateUserDto;
    }

    public User userFromDto(RegisterDto registerDto) {
        User user = new User();
        registerDtoToObject(registerDto, user);
        return user;
    }

    private void registerDtoToObject(RegisterDto registerDto, User user) {
        Role role = roleRepository.getByName(USER_ROLE);

        user.setUsername(registerDto.getUsername());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setPassword(registerDto.getPassword());
        user.setRole(role);
    }

    private void userDtoToObject(UserDto userDto, User user) {
        Role role = roleRepository.getByName(USER_ROLE);

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPassword(userDto.getPassword());
        user.setUsername(userDto.getUsername());
        user.setRole(role);
    }

    private void userDtoToObject(UpdateUserDto updateUserDto, User user) {
        user.setFirstName(updateUserDto.getFirstName());
        user.setLastName(updateUserDto.getLastName());
        user.setPassword(updateUserDto.getPassword());

        if (updateUserDto.isPromotedToOrganiser()) {
            user.setRole(roleRepository.getByName(ORGANISER_ROLE));
        }

    }

    public Ranking generateUserRanking(User user) {
        Ranking ranking = new Ranking();
        ranking.setTotalScore(DEFAULT_USER_SCORE);
        ranking.setUser(user);
        return ranking;
    }

    public Ranking rankingFromDto(RankingDto rankingDto, User rankingUser) {
        Ranking rankingToUpdate = rankingRepository.getByUser(rankingUser.getUsername());
        setRankingScore(rankingDto, rankingToUpdate);

        return rankingToUpdate;
    }

    private void setRankingScore(RankingDto rankingDto, Ranking rankingToUpdate) {
        if (rankingDto.getNewTotalScore() != null) {
            rankingToUpdate.setTotalScore(rankingDto.getNewTotalScore());
        } else if (rankingDto.getPointsToAdd() != null) {
            rankingToUpdate.setTotalScore(rankingToUpdate.getTotalScore() + rankingDto.getPointsToAdd());
        } else if (rankingDto.getPointsToRemove() != null) {
            rankingToUpdate.setTotalScore(rankingToUpdate.getTotalScore() - rankingDto.getPointsToRemove());
        }
    }
}
