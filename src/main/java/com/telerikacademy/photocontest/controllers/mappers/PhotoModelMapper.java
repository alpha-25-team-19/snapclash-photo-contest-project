package com.telerikacademy.photocontest.controllers.mappers;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.transfer.PhotoDto;
import com.telerikacademy.photocontest.repositories.ContestRepository;
import com.telerikacademy.photocontest.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PhotoModelMapper {

    private final UserRepository userRepository;
    private final ContestRepository contestRepository;

    @Autowired
    public PhotoModelMapper(UserRepository userRepository,
                            ContestRepository contestRepository) {
        this.userRepository = userRepository;
        this.contestRepository = contestRepository;
    }

    public Photo fromDto(PhotoDto photoDto) {
        Photo photo = new Photo();
        dtoToObject(photo, photoDto);
        return photo;
    }

    private void dtoToObject(Photo photo, PhotoDto photoDto) {
        User photoOwner = userRepository.getById(photoDto.getUserId());
        Contest photoContest = contestRepository.getById(photoDto.getContestId());

        photo.setTitle(photoDto.getTitle());
        photo.setStory(photoDto.getStory());
        photo.setPhotoUser(photoOwner);
        photo.setContest(photoContest);
    }


}
