package com.telerikacademy.photocontest.controllers.mappers;

import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.PhotoReview;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.transfer.PhotoReviewDto;
import com.telerikacademy.photocontest.repositories.PhotoRepository;
import com.telerikacademy.photocontest.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.telerikacademy.photocontest.utils.GlobalConstants.INVALID_CATEGORY_PHOTO_SCORE;

@Component
public class PhotoReviewMapper {

    private final PhotoRepository photoRepository;
    private final UserRepository userRepository;

    @Autowired
    public PhotoReviewMapper(PhotoRepository photoRepository, UserRepository userRepository) {
        this.photoRepository = photoRepository;
        this.userRepository = userRepository;
    }

    public PhotoReview fromDto(PhotoReviewDto photoReviewDto) {
        PhotoReview photoReview = new PhotoReview();
        dtoToObject(photoReview, photoReviewDto);

        return photoReview;
    }

    private void dtoToObject(PhotoReview photoReview, PhotoReviewDto photoReviewDto) {
        User juror = userRepository.getById(photoReviewDto.getJurorId());
        Photo photo = photoRepository.getById(photoReviewDto.getPhotoId());

        photoReview.setJuror(juror);
        photoReview.setPhoto(photo);
        photoReview.setPhotoComment(photoReviewDto.getComment());

        if (photoReviewDto.isInvalidCategory()) {
            photoReview.setPhotoScore(INVALID_CATEGORY_PHOTO_SCORE);
        } else {
            photoReview.setPhotoScore(photoReviewDto.getPhotoScore());
        }

        photoReview.setInvalidCategory(photoReviewDto.isInvalidCategory());
    }

}
