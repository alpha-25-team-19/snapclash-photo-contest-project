package com.telerikacademy.photocontest.controllers.rest;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.PhotoReviewMapper;
import com.telerikacademy.photocontest.models.PhotoReview;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.transfer.PhotoReviewDto;
import com.telerikacademy.photocontest.services.PhotoReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(description = "Creating and Retrieving Photo reviews", tags = {"photo-reviews"})
@RestController
@RequestMapping("/snapclash/photo-reviews")
public class PhotoReviewRestController {

    private final PhotoReviewService photoReviewService;
    private final AuthenticationHelper authenticationHelper;
    private final PhotoReviewMapper photoReviewMapper;

    @Autowired
    public PhotoReviewRestController(PhotoReviewService photoReviewService,
                                     AuthenticationHelper authenticationHelper,
                                     PhotoReviewMapper photoReviewMapper) {
        this.photoReviewService = photoReviewService;
        this.authenticationHelper = authenticationHelper;
        this.photoReviewMapper = photoReviewMapper;
    }

    @ApiOperation(value = "Retrieve all photo reviews")
    @GetMapping
    public List<PhotoReview> getAll() {
        return photoReviewService.getAll();
    }

    @ApiOperation(value = "Retrieve a photo review by id")
    @GetMapping("{id}")
    public PhotoReview getById(@PathVariable int id) {
        return photoReviewService.getById(id);
    }

    @ApiOperation(value = "Create a photo review")
    @PostMapping()
    public PhotoReview create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PhotoReviewDto photoReviewDto) {
        User user = authenticationHelper.tryGetUser(headers);
        PhotoReview photoReview = photoReviewMapper.fromDto(photoReviewDto);

        photoReviewService.createPhotoReview(photoReview, user);

        return photoReview;
    }

    @ApiOperation(value = "Delete a photo review")
    @DeleteMapping("{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        photoReviewService.deletePhotoReview(id, user);
    }
}
