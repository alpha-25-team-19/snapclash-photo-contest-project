package com.telerikacademy.photocontest.controllers.rest;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.PhotoModelMapper;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.payload.UploadFileResponse;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.models.transfer.PhotoDto;
import com.telerikacademy.photocontest.services.FileStorageService;
import com.telerikacademy.photocontest.services.PhotoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(description = "Creating and Retrieving Photos", tags = {"photos"})
@RestController
@RequestMapping("/snapclash/photos")
public class PhotoRestController {

    private final PhotoService photoService;
    private final PhotoModelMapper photoModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final FileStorageService fileStorageService;

    @Autowired
    public PhotoRestController(PhotoService photoService,
                               PhotoModelMapper photoModelMapper,
                               AuthenticationHelper authenticationHelper,
                               FileStorageService fileStorageService) {
        this.photoService = photoService;
        this.photoModelMapper = photoModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.fileStorageService = fileStorageService;
    }

    @ApiOperation(value = "Retrieve all photos")
    @GetMapping
    public List<Photo> getAll() {
        return photoService.getAll();
    }

    @ApiOperation(value = "Retrieve a photo by id")
    @GetMapping("/{id}")
    public Photo getById(@PathVariable int id) {
        return photoService.getById(id);
    }

    @ApiOperation(value = "Filter photos by contest title and/or username")
    @GetMapping("filter")
    public List<Photo> filter(@RequestParam(required = false) Optional<String> contest,
                              @RequestParam(required = false) Optional<String> username) {
        return photoService.filter(new PhotoSearchParameters(contest.orElse(""), username.orElse("")));
    }

    @ApiOperation(value = "Create a new Photo")
    @PostMapping()
    public Photo createPhoto(@RequestHeader HttpHeaders headers, @Valid @RequestBody PhotoDto photoDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Photo photo = photoModelMapper.fromDto(photoDto);

        photoService.createPhoto(user, photo);
        return photo;
    }

    @ApiOperation(value = "Upload a single photo")
    @PostMapping("{id}/upload")
    public UploadFileResponse uploadFile(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestParam("file") MultipartFile file) {
        User user = authenticationHelper.tryGetUser(headers);
        Photo photo = photoService.getById(id);

        String fileName = fileStorageService.storeFile(file, photo, user);
        return new UploadFileResponse(fileName, file.getContentType(), file.getSize());
    }

}
