package com.telerikacademy.photocontest.controllers.rest;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.UserModelMapper;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.search.UserSearchParameters;
import com.telerikacademy.photocontest.models.transfer.RankingDto;
import com.telerikacademy.photocontest.models.transfer.UpdateUserDto;
import com.telerikacademy.photocontest.models.transfer.UserDto;
import com.telerikacademy.photocontest.services.RankingService;
import com.telerikacademy.photocontest.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(description = "Creating, Updating and Retrieving Users", tags = {"users"})
@RestController
@RequestMapping("/snapclash/users")
public class UserRestController {

    private final UserService userService;
    private final RankingService rankingService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;

    @Autowired
    public UserRestController(UserService userService,
                              RankingService rankingService,
                              AuthenticationHelper authenticationHelper,
                              UserModelMapper userModelMapper) {
        this.userService = userService;
        this.rankingService = rankingService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
    }

    @ApiOperation(value = "Retrieves all Users")
    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getAll(user);
    }

    @ApiOperation(value = "Retrieves a User by their id")
    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        User user = authenticationHelper.tryGetUser(headers);

        return userService.getById(id, user);
    }

    @ApiOperation(value = "Retrieves all User rankings")
    @GetMapping("/rankings")
    public List<Ranking> getAllRankings(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return rankingService.getAll(user);
    }

    @ApiOperation(value = "Retrieves a User's ranking by their id")
    @GetMapping("/{id}/ranking")
    public Ranking getUserRanking(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        User rankingUser = userService.getById(id, user);
        return rankingService.getByUser(rankingUser, user);
    }

    @ApiOperation(value = "Creates a new User with a Ranking score of 0")
    @PostMapping()
    public User createUser(@Valid @RequestBody UserDto userDto) {
        User user = userModelMapper.userFromDto(userDto);
        Ranking ranking = userModelMapper.generateUserRanking(user);
        userService.createUser(user, ranking);

        return user;
    }

    @ApiOperation(value = "Update User information or promotes them to Organiser role")
    @PutMapping("{id}")
    public User updateUser(@RequestHeader HttpHeaders headers,
                           @PathVariable int id,
                           @Valid @RequestBody UpdateUserDto updateUserDto) {

        User user = authenticationHelper.tryGetUser(headers);
        User userToUpdate = userModelMapper.userFromDto(updateUserDto, id);
        userService.update(userToUpdate, user);

        return userToUpdate;
    }

    @ApiOperation(value = "Filter users by name and/or ranking")
    @GetMapping("/filter")
    public List<Ranking> filter(@RequestHeader HttpHeaders headers,
                                @RequestParam(required = false) Optional<String> name,
                                @RequestParam(required = false) Optional<String> ranking) {

        User user = authenticationHelper.tryGetUser(headers);
        return userService.filter(
                new UserSearchParameters(name.orElse(""), ranking.orElse("")), user);
    }

    @ApiOperation(value = "Update User ranking")
    @PutMapping("/{id}/ranking")
    public Ranking updateUserRanking(@RequestHeader HttpHeaders headers,
                                     @PathVariable int id,
                                     @Valid @RequestBody RankingDto rankingDto) {

        User user = authenticationHelper.tryGetUser(headers);
        User rankingUser = userService.getById(id, user);
        Ranking ranking = userModelMapper.rankingFromDto(rankingDto, rankingUser);
        rankingService.updateRanking(ranking, rankingUser, user);

        return ranking;
    }
}
