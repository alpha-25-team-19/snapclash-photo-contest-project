package com.telerikacademy.photocontest.controllers.rest;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.mappers.ContestModelMapper;
import com.telerikacademy.photocontest.controllers.mappers.LeaderboardModelMapper;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestLeaderboard;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.transfer.ContestDto;
import com.telerikacademy.photocontest.services.ContestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(description = "Creates and Retrieves Contests", tags = {"contests"})
@RestController
@RequestMapping("/snapclash/contests")
public class ContestRestController {

    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;
    private final LeaderboardModelMapper leaderboardModelMapper;
    private final ContestModelMapper contestModelMapper;

    @Autowired
    public ContestRestController(ContestService contestService,
                                 AuthenticationHelper authenticationHelper,
                                 LeaderboardModelMapper leaderboardModelMapper,
                                 ContestModelMapper contestModelMapper) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.leaderboardModelMapper = leaderboardModelMapper;
        this.contestModelMapper = contestModelMapper;
    }

    @ApiOperation(value = "Retrieves all Contests")
    @GetMapping
    public List<Contest> getAll() {
        return contestService.getAll();
    }

    @ApiOperation(value = "Retrieves a Contest by id")
    @GetMapping("/{id}")
    public Contest getById(@PathVariable int id) {
        return contestService.getById(id);
    }

    @ApiOperation(value = "Retrieves a Contest by multiple criteria")
    @GetMapping("/filter")
    public List<Contest> filter(@RequestHeader HttpHeaders headers,
                                @RequestParam(required = false) Optional<String> phase,
                                @RequestParam(required = false) Optional<String> contestCategory,
                                @RequestParam(required = false) Optional<String> contestType,
                                @RequestParam(required = false) Optional<String> title,
                                @RequestParam(required = false) Optional<Integer> userId) {

        User user = authenticationHelper.tryGetUser(headers);

        return contestService.filter(
                new ContestSearchParameters(phase.orElse(""),
                        title.orElse(""),
                        contestType.orElse(""),
                        contestCategory.orElse(""),
                        userId.orElse(-1)), user);
    }

    @ApiOperation(value = "Get contest leaderboard")
    @GetMapping("/{id}/leaderboard")
    public ContestLeaderboard getContestLeaderboard(@PathVariable int id) {
        return leaderboardModelMapper.fromContestId(id);
    }

    @ApiOperation(value = "Allocates the point for the first three spots once the contest is finished")
    @PutMapping("/{id}/leaderboard")
    public ContestLeaderboard allocateLeaderboardPoints(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        ContestLeaderboard contestLeaderboard = leaderboardModelMapper.fromContestId(id);
        contestService.allocateContestPoints(contestLeaderboard, user);

        return contestLeaderboard;
    }

    @ApiOperation(value = "Creates a new contest")
    @PostMapping
    public void create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody ContestDto contestDto) {

        User organizer = authenticationHelper.tryGetUser(headers);
        Contest contest = contestModelMapper.fromDto(contestDto);
        contestService.createContest(organizer, contest);
    }

}
