package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.PhotoReview;
import com.telerikacademy.photocontest.models.User;

import java.util.List;

public interface PhotoReviewService {

    List<PhotoReview> getAll();

    List<PhotoReview> getByPhoto(int photoId);

    PhotoReview getById(int id);

    void createPhotoReview(PhotoReview photoReview, User user);

    void deletePhotoReview(int id, User user);

    double getPhotoFinalScore(Photo photo);
}
