package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.InvalidContestPhaseException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.PhotoReview;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.PhotoReviewRepository;
import com.telerikacademy.photocontest.utils.AuthorizationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@Service
public class PhotoReviewServiceImpl implements PhotoReviewService {

    private final PhotoReviewRepository photoReviewRepository;

    @Autowired
    public PhotoReviewServiceImpl(PhotoReviewRepository photoReviewRepository) {
        this.photoReviewRepository = photoReviewRepository;
    }

    @Override
    public List<PhotoReview> getAll() {
        return photoReviewRepository.getAll();
    }

    @Override
    public List<PhotoReview> getByPhoto(int photoId) {
        return photoReviewRepository.getByPhoto(photoId);
    }

    @Override
    public PhotoReview getById(int id) {
        return photoReviewRepository.getById(id);
    }

    @Override
    public void createPhotoReview(PhotoReview photoReview, User user) {
        Contest currentContest = photoReview.getPhoto().getContest();
        checkContestPhase(photoReview.getPhoto().getContest(), CONTEST_PHASE_TWO, CONTEST_PHASE_TWO_ERROR_MESSAGE);
        checkJurorSubmission(photoReview.getJuror(), user);
        checkJuryAuthorization(user, currentContest);
        checkReviewDuplication(photoReview, currentContest);

        photoReviewRepository.create(photoReview);
    }

    @Override
    public double getPhotoFinalScore(Photo photo) {
        checkContestPhase(photo.getContest(), CONTEST_PHASE_FINISHED, CONTEST_FINISHED_PHASE_ERROR_MESSAGE);
        checkJuryReviewCount(photo);
        return photoReviewRepository.getPhotoFinalScore(photo.getId());

    }

    private void checkContestPhase(Contest contest, String phase, String errorMessage) {
        if (!contest.getContestPhase().equalsIgnoreCase(phase)) {
            throw new InvalidContestPhaseException(errorMessage);
        }
    }

    private void checkJuryReviewCount(Photo photo) {
        if (photo.getContest().getJury().size() > photoReviewRepository.getPhotoReviewCount(photo.getId())) {
            List<PhotoReview> currentPhotoReviews = photoReviewRepository.getByPhoto(photo.getId());
            List<User> reviewers = getCurrentReviewers(currentPhotoReviews);
            generateDefaultReviews(photo, reviewers);
        }
    }

    private List<User> getCurrentReviewers(List<PhotoReview> currentPhotoReviews) {
        List<User> reviewers = new ArrayList<>();

        if (!currentPhotoReviews.isEmpty()) {
            for (PhotoReview photoReview : currentPhotoReviews) {
                reviewers.add(photoReview.getJuror());
            }
        }
        return reviewers;
    }

    private void generateDefaultReviews(Photo photo, List<User> reviewers) {
        for (User juror : photo.getContest().getJury()) {
            if (!reviewers.contains(juror)) {
                PhotoReview defaultReview = new PhotoReview();
                defaultReview.setPhoto(photo);
                defaultReview.setJuror(juror);
                defaultReview.setPhotoScore(DEFAULT_PHOTO_REVIEW_SCORE);
                defaultReview.setPhotoComment(DEFAULT_PHOTO_REVIEW_COMMENT);
                photoReviewRepository.create(defaultReview);
            }
        }
    }

    @Override
    public void deletePhotoReview(int id, User user) {
        PhotoReview photoReviewToDelete = photoReviewRepository.getById(id);
        int jurorId = photoReviewToDelete.getJuror().getId();

        AuthorizationChecker.checkUserAuthorization(jurorId, user, DELETE_REVIEW_AUTHORIZATION_ERROR_MESSAGE);
        photoReviewRepository.delete(photoReviewToDelete);
    }

    private void checkJurorSubmission(User juror, User user) {
        if (!juror.equals(user)) {
            throw new UnauthorizedOperationException(INVALID_JURY_SUBMISSION_ERROR_MESSAGE);
        }
    }

    private static void checkJuryAuthorization(User user, Contest currentContest) {
        if (!currentContest.getJury().contains(user)) {
            throw new UnauthorizedOperationException(String.format(PHOTO_REVIEW_INVALID_JURY_MESSAGE,
                    user.getUsername(), currentContest.getTitle()));
        }
    }

    private void checkReviewDuplication(PhotoReview photoReview, Contest currentContest) {
        List<PhotoReview> jurorReviews = photoReviewRepository.getByJuror(photoReview.getJuror().getId());

        for (PhotoReview jurorReview : jurorReviews) {
            if (jurorReview.getPhoto().getContest().getId() == currentContest.getId()
                    && jurorReview.getPhoto().getId() == photoReview.getPhoto().getId()) {
                throw new DuplicateEntityException(REVIEW_ALREADY_EXISTS);
            }
        }
    }
}
