package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.ContestCategory;
import com.telerikacademy.photocontest.repositories.ContestCategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContestCategoryServiceImpl implements ContestCategoryService {

    private final ContestCategoryRepository contestCategoryRepository;

    public ContestCategoryServiceImpl(ContestCategoryRepository contestCategoryRepository) {
        this.contestCategoryRepository = contestCategoryRepository;
    }

    @Override
    public List<ContestCategory> getAll() {
        return contestCategoryRepository.getAll();
    }

    @Override
    public ContestCategory getById(int id) {
        return contestCategoryRepository.getById(id);
    }
}
