package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.*;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.enums.RankType;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.repositories.*;
import com.telerikacademy.photocontest.utils.AuthorizationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;
import static java.time.temporal.ChronoUnit.*;

@Service
public class ContestServiceImpl implements ContestService {

    private final ContestRepository contestRepository;
    private final RankingRepository rankingRepository;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, RankingRepository rankingRepository) {
        this.contestRepository = contestRepository;
        this.rankingRepository = rankingRepository;
    }

    @Override
    public List<Contest> getAll() {
        return contestRepository.getAll();
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    @Override
    public void createContest(User organizer, Contest contest) {
        AuthorizationChecker.checkOrganiserAuthorization(organizer, ORGANISER_CREATE_CONTEST_AUTHORIZATION_ERROR_MESSAGE);

        checkDuplicate(contest);
        checkContestTimetable(contest);
        checkJuryEligibility(contest);

        if (contest.getContestType().getTypeName().equalsIgnoreCase(CONTEST_TYPE_INVITATION)) {
            checkInvitations(contest);
        }

        contestRepository.create(contest);
    }

    @Override
    public List<Contest> filter(ContestSearchParameters contestSearchParameters, User user) {
        if (user.getId() != contestSearchParameters.getUserId()
                && !user.getRole().getRoleName().equalsIgnoreCase(ORGANISER_ROLE)) {
            throw new UnauthorizedOperationException(USER_FILTER_AUTHORIZATION_ERROR_MESSAGE);
        }

        return contestRepository.filter(contestSearchParameters);
    }

    public void allocateContestPoints(ContestLeaderboard contestLeaderboard, User user) {
        AuthorizationChecker.checkOrganiserAuthorization(user, ORGANISER_CONTEST_AWARDS_AUTHORIZATION_ERROR_MESSAGE);
        checkIfAwarded(contestLeaderboard.getContest());
        updateUserRankings(contestLeaderboard);
        contestLeaderboard.getContest().setAwarded(true);
        contestRepository.update(contestLeaderboard.getContest());
    }

    private void checkDuplicate(Contest contest) {
        boolean duplicateExists = true;

        try {
            contestRepository.getByTitle(contest.getTitle());
        } catch (EntityNotFoundException exc) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(CONTEST_ALREADY_EXISTS);
        }
    }

    private void checkInvitations(Contest contest) {
            for (User user : contest.getInvitedUsers()) {
                if (user.getRole().getRoleName().equalsIgnoreCase(ORGANISER_ROLE) || contest.getJury().contains(user)) {
                    throw new UnauthorizedOperationException(PARTICIPANT_CONTEST_AUTHORIZATION_ERROR_MESSAGE);
                }
            }
    }

    private void checkJuryEligibility(Contest contest) {

        List<String> eligibleJuryTypes = new ArrayList<>();
        eligibleJuryTypes.add(RankType.PHOTO_MASTER.toString());
        eligibleJuryTypes.add(RankType.WISE_AND_BENEVOLENT_PHOTO_DICTATOR.toString());

        for (User user : contest.getJury()) {
            String userRole = user.getRole().getRoleName();

            if (!userRole.equalsIgnoreCase(ORGANISER_ROLE)) {
                String userRanking = rankingRepository.getByUser(user.getUsername()).getRankType();

                if (!eligibleJuryTypes.contains(userRanking)) {
                    throw new UnauthorizedOperationException(ORGANISER_CONTEST_JURY_AUTHORIZATION_ERROR_MESSAGE);
                }
            }
        }
    }

    private void checkContestTimetable(Contest contest) {

        LocalDateTime contestPhaseOneStart = contest.getPhaseOneStart();
        LocalDateTime contestPhaseTwoStart = contest.getPhaseTwoStart();
        LocalDateTime contestEnd = contest.getContestEnd();

        checkStartBeforeEnd(contestPhaseOneStart, contestPhaseTwoStart, CONTEST_PHASE_ONE_DATE_VALIDATION_ERROR_MESSAGE);
        checkStartBeforeEnd(contestPhaseTwoStart, contestEnd, CONTEST_PHASE_TWO_DATE_VALIDATION_ERROR_MESSAGE);

        checkPhaseTimespan(contestPhaseOneStart, contestPhaseTwoStart, DAYS,
                MIN_PHASE_ONE_DAYS, MAX_PHASE_ONE_DAYS,
                String.format(PHASE_ONE_TIMESPAN_VALIDATION_ERROR_MESSAGE,
                        MIN_PHASE_ONE_DAYS, MAX_PHASE_ONE_DAYS));

        checkPhaseTimespan(contestPhaseTwoStart, contestEnd, HOURS,
                MIN_PHASE_TWO_HOURS, MAX_PHASE_TWO_HOURS,
                String.format(PHASE_TWO_TIMESPAN_VALIDATION_ERROR_MESSAGE,
                        MIN_PHASE_TWO_HOURS, MAX_PHASE_TWO_HOURS));
    }

    private void checkStartBeforeEnd(LocalDateTime start, LocalDateTime end, String message) {
        if (start.isAfter(end)) {
            throw new InvalidPeriodException(message);
        }
    }

    private void checkPhaseTimespan(LocalDateTime start,
                                    LocalDateTime end,
                                    ChronoUnit temporalUnit,
                                    int minTimespan,
                                    int maxTimespan,
                                    String message) {

        if (start.until(end, temporalUnit) > maxTimespan || start.until(end, temporalUnit) < minTimespan) {
            throw new InvalidPeriodException(message);
        }
    }

    private void checkIfAwarded(Contest contest) {
        if (contest.isAwarded()) {
            throw new DuplicateEntityException(CONTEST_POINTS_ALREADY_ALLOCATED);
        }
    }

    private void updateUserRankings(ContestLeaderboard contestLeaderboard) {
        HashMap<String, Double> firstPlaceWinners = new HashMap<>(contestLeaderboard.getFirstPlaceWinners());
        HashMap<String, Double> secondPlaceWinners = new HashMap<>(contestLeaderboard.getSecondPlaceWinners());
        HashMap<String, Double> thirdPlaceWinners = new HashMap<>(contestLeaderboard.getThirdPlaceWinners());

        // Checks if 1st place' score is double or more that of 2nd place and updates ranking

        boolean isDoubleLead = updateUserRankingsIfDoubleLead(firstPlaceWinners, secondPlaceWinners);
        if (!isDoubleLead) {
            updateUserRankings(firstPlaceWinners, FIRST_PLACE_SINGLE_SCORE, FIRST_PLACE_SHARED_SCORE);
        }

        // Checks if 1st, 2nd or 3rd place are ties or not and updates rankings

        updateUserRankings(secondPlaceWinners, SECOND_PLACE_SINGLE_SCORE, SECOND_PLACE_SHARED_SCORE);
        updateUserRankings(thirdPlaceWinners, THIRD_PLACE_SINGLE_SCORE, THIRD_PLACE_SHARED_SCORE);

    }

    private void updateUserRankings(HashMap<String, Double> winnersMap, int singleScore, int sharedScore) {

        if (winnersMap.size() == 1) {
            Ranking rankingToUpdate = rankingRepository.getByUser(winnersMap.keySet().stream().findFirst().get());
            int currentScore = rankingToUpdate.getTotalScore();

            rankingToUpdate.setTotalScore(currentScore + singleScore);
            rankingRepository.update(rankingToUpdate);
        } else {
            int counter = winnersMap.size();
            Set<String> winnersUsernames = winnersMap.keySet();

            while (counter > 0) {
                String tempUsername = winnersUsernames.stream().findFirst().get();
                Ranking tempRanking = rankingRepository.getByUser(tempUsername);
                int tempScore = tempRanking.getTotalScore();

                tempRanking.setTotalScore(tempScore + sharedScore);
                rankingRepository.update(tempRanking);
                winnersUsernames.remove(tempUsername);
                counter--;
            }
        }
    }

    private boolean updateUserRankingsIfDoubleLead(HashMap<String, Double> firstPlaceWinners,
                                                   HashMap<String, Double> secondPlaceWinners) {
        Ranking rankingToUpdate = rankingRepository.getByUser(firstPlaceWinners.keySet().stream().findFirst().get());
        int currentScore = rankingToUpdate.getTotalScore();

        if (firstPlaceWinners.size() == 1
                && firstPlaceWinners.values().stream().findFirst().get() >= secondPlaceWinners.values().stream().findFirst().get() * 2) {

            rankingToUpdate.setTotalScore(currentScore + FIRST_PLACE_DOUBLE_SCORE);
            rankingRepository.update(rankingToUpdate);
            return true;
        } else {
            return false;
        }
    }
}


