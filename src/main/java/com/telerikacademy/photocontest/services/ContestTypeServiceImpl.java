package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.ContestType;
import com.telerikacademy.photocontest.repositories.ContestTypeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContestTypeServiceImpl implements ContestTypeService {

    private final ContestTypeRepository contestTypeRepository;

    public ContestTypeServiceImpl(ContestTypeRepository contestTypeRepository) {
        this.contestTypeRepository = contestTypeRepository;
    }

    @Override
    public List<ContestType> getAll() {
        return contestTypeRepository.getAll();
    }

    @Override
    public ContestType getById(int id) {
        return contestTypeRepository.getById(id);
    }
}
