package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.ContestType;

import java.util.List;

public interface ContestTypeService {

    List<ContestType> getAll();

    ContestType getById(int id);

}
