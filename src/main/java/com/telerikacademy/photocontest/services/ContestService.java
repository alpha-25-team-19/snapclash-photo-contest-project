package com.telerikacademy.photocontest.services;


import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestLeaderboard;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.models.User;

import java.util.List;

public interface ContestService {

    List<Contest> getAll();

    Contest getById(int id);

    void createContest(User organizer, Contest contest);

    void allocateContestPoints(ContestLeaderboard contestLeaderboard, User user);

    List<Contest> filter(ContestSearchParameters contestSearchParameters, User user);


}
