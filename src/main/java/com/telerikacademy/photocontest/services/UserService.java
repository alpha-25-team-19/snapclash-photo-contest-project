package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.search.UserSearchParameters;

import java.util.List;

public interface UserService {

    List<User> getAll(User user);

    List<Ranking> filter(UserSearchParameters userSearchParameters, User user);

    User getById(int id, User user);

    User getByUsername(String username);

    List<User> getAllEligibleJurors();

    void createUser(User user, Ranking ranking);

    void update(User userToUpdate, User user);

}
