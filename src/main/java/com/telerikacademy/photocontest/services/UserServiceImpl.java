package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.search.UserSearchParameters;
import com.telerikacademy.photocontest.repositories.RankingRepository;
import com.telerikacademy.photocontest.repositories.UserRepository;
import com.telerikacademy.photocontest.utils.AuthorizationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RankingRepository rankingRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RankingRepository rankingRepository) {
        this.userRepository = userRepository;
        this.rankingRepository = rankingRepository;
    }

    @Override
    public List<User> getAll(User user) {
        AuthorizationChecker.checkOrganiserAuthorization(user, ORGANISER_READ_AUTHORIZATION_ERROR_MESSAGE);
        return userRepository.getAll();
    }

    @Override
    public User getById(int id, User user) {
        AuthorizationChecker.checkUserAuthorization(id, user, USER_READ_AUTHORIZATION_ERROR_MESSAGE);
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public List<User> getAllEligibleJurors() {
        List<User> eligibleJurors = new ArrayList<>();
        eligibleJurors.addAll(userRepository.getAllOrganisers());
        eligibleJurors.addAll(userRepository.getAllEligibleJurors());
        return eligibleJurors;
    }

    @Override
    public void createUser(User user, Ranking ranking) {
        checkDuplicateUser(user);

        userRepository.create(user);
        rankingRepository.create(ranking);
    }

    @Override
    public void update(User userToUpdate, User user) {
        checkPromotionAuthorization(userToUpdate, user, USER_PROMOTION_AUTHORIZATION_ERROR_MESSAGE);
        AuthorizationChecker.checkUserAuthorization(userToUpdate.getId(), user, USER_UPDATE_AUTHORIZATION_ERROR_MESSAGE);
        checkDuplicateUserUpdate(userToUpdate);
        deleteRankingIfPromoted(userToUpdate);

        userRepository.update(userToUpdate);
    }

    @Override
    public List<Ranking> filter(UserSearchParameters userSearchParameters, User user) {
        AuthorizationChecker.checkOrganiserAuthorization(user, ORGANISER_READ_AUTHORIZATION_ERROR_MESSAGE);
        return userRepository.filter(userSearchParameters);
    }

    private void checkPromotionAuthorization(User userToUpdate, User user, String message) {
        if (userToUpdate.getRole().getRoleName().equals(ORGANISER_ROLE)
                && user.getRole().getRoleName().equals(USER_ROLE)) {
            throw new UnauthorizedOperationException(message);
        }
    }

    private void checkDuplicateUser(User user) {
        boolean duplicateExists = true;

        try {
            userRepository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException exc) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(USER_ALREADY_EXISTS);
        }
    }


    private void checkDuplicateUserUpdate(User userToUpdate) {
        boolean duplicateExists = true;

        try {
            User existingUser = userRepository.getByUsername(userToUpdate.getUsername());
            if (existingUser.getId() == userToUpdate.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException exc) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(USER_ALREADY_EXISTS);
        }
    }

    private void deleteRankingIfPromoted(User userToUpdate) {
        if (userToUpdate.getRole().getRoleName().equals(ORGANISER_ROLE)) {
            try {
                Ranking rankingToDelete = rankingRepository.getByUser(userToUpdate.getUsername());
                rankingRepository.delete(rankingToDelete);
            } catch (EntityNotFoundException ignored) {

            }
        }
    }

}
