package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.FileStorageException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.PhotoUrl;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.properties.FileStorageProperties;
import com.telerikacademy.photocontest.repositories.FileStorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    private final Path fileStorageLocation;
    private final FileStorageRepository fileStorageRepository;
    private final FileStorageProperties fileStorageProperties;

    @Autowired
    public FileStorageServiceImpl(FileStorageProperties fileStorageProperties,
                                  FileStorageRepository fileStorageRepository,
                                  FileStorageProperties fileStorageProperties1) {
        this.fileStorageLocation = Paths.get("./src/main/resources/static"
                + fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
        this.fileStorageRepository = fileStorageRepository;
        this.fileStorageProperties = fileStorageProperties1;

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception exc) {
            throw new FileStorageException(CREATE_DIRECTORY_ERROR_MESSAGE, exc);
        }
    }

    @Override
    public String storeFile(MultipartFile multipartFile, Photo photo, User user) {
        checkUserUpload(photo, user);

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

        try {
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(multipartFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            checkDuplicatePhoto(photo);
            fileStorageRepository.saveFilePath(generatePhotoUrl(photo, fileName));

            return fileName;
        } catch (IOException exc) {
            throw new FileStorageException(String.format(FILE_STORAGE_ERROR_MESSAGE, fileName), exc);
        }
    }

    @Override
    public List<PhotoUrl> getAllPhotoUrls() {
        return fileStorageRepository.getAll();
    }

    @Override
    public PhotoUrl getPhotoUrlById(int id) {
        return fileStorageRepository.getById(id);
    }

    private void checkDuplicatePhoto(Photo photo) {
        boolean duplicateExists = true;

        try {
            fileStorageRepository.getByPhoto(photo.getId());
        } catch (EntityNotFoundException exc) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(PHOTO_FILE_ALREADY_UPLOADED);
        }
    }

    private PhotoUrl generatePhotoUrl(Photo photo, String fileName) {
        PhotoUrl photoUrl = new PhotoUrl();
        photoUrl.setPhotoUrl(fileStorageProperties.getUploadDir() + "/" + fileName);
        photoUrl.setPhoto(photo);
        return photoUrl;
    }

    private void checkUserUpload(Photo photo, User user) {
        User photoUser = photo.getPhotoUser();

        if (!user.equals(photoUser)) {
            throw new UnauthorizedOperationException(INVALID_USER_PHOTO_UPLOAD_ERROR_MESSAGE);
        }
    }
}
