package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.PhotoUrl;
import com.telerikacademy.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileStorageService {

    String storeFile(MultipartFile multipartFile, Photo photo, User user);

    List<PhotoUrl> getAllPhotoUrls();

    PhotoUrl getPhotoUrlById(int id);
}
