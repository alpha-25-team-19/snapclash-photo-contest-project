package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;

import java.util.List;

public interface RankingService {

    List<Ranking> getAll(User user);

    Ranking getByUser(User rankingUser, User user);

    void updateRanking(Ranking rankingToUpdate, User rankingUser, User user);
}
