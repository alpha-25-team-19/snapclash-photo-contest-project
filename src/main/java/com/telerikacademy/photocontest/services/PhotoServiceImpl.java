package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.enums.ContestPhase;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.repositories.PhotoRepository;
import com.telerikacademy.photocontest.repositories.RankingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;
    private final RankingRepository rankingRepository;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository, RankingRepository rankingRepository) {
        this.photoRepository = photoRepository;
        this.rankingRepository = rankingRepository;
    }

    @Override
    public List<Photo> getAll() {
        return photoRepository.getAll();
    }

    @Override
    public List<Photo> filter(PhotoSearchParameters photoSearchParameters) {
        return photoRepository.filter(photoSearchParameters);
    }

    @Override
    public Photo getById(int id) {
        return photoRepository.getById(id);
    }

    @Override
    public void createPhoto(User user, Photo photo) {
        checkUserSubmissionAuthorization(user, photo);
        checkOrganiserSubmission(user);
        checkContestPhase(photo);
        checkIfJury(photo, user);
        checkUserInvitation(user, photo);
        checkMultipleSubmissions(photo);
        awardPointsForParticipation(photo);

        photoRepository.create(photo);
    }

    private void awardPointsForParticipation(Photo photo) {
        Ranking currentRanking = rankingRepository.getByUser(photo.getPhotoUser().getUsername());
        int currentScore = currentRanking.getTotalScore();

        if (photo.getContest().getContestType().getTypeName().equals(CONTEST_TYPE_INVITATION)) {
            currentRanking.setTotalScore(currentScore + DEFAULT_POINTS_FOR_INVITATIONAL_CONTEST);
        } else {
            currentRanking.setTotalScore(currentScore + DEFAULT_POINTS_FOR_OPEN_CONTEST);
        }
        rankingRepository.update(currentRanking);
    }

    private void checkContestPhase(Photo photo) {
        Contest contest = photo.getContest();

        if (!contest.getContestPhase().equals(ContestPhase.PHASE_ONE.toString())) {
            throw new UnauthorizedOperationException(CONTEST_SUBMISSION_INVALID_PHASE);
        }
    }

    private void checkMultipleSubmissions(Photo photo) {
        List<Photo> contestPhotos;

        try {
            contestPhotos = photoRepository.filter(new PhotoSearchParameters(photo.getContest().getTitle(), ""));
        } catch (EntityNotFoundException exc) {
            return;
        }

        if (contestPhotos.stream()
                .anyMatch(contestPhoto -> contestPhoto.getPhotoUser().equals(photo.getPhotoUser()))) {
            throw new UnauthorizedOperationException(USER_DUPLICATE_PHOTO_SUBMISSION_ERROR_MESSAGE);
        }
    }

    private void checkUserSubmissionAuthorization(User user, Photo photo) {
        if (!photo.getPhotoUser().equals(user)) {
            throw new UnauthorizedOperationException(INVALID_USER_PHOTO_SUBMISSION_ERROR_MESSAGE);
        }
    }

    private void checkOrganiserSubmission(User user) {
        if (user.getRole().getRoleName().equals(ORGANISER_ROLE)) {
            throw new UnauthorizedOperationException(ORGANISER_SUBMISSION_ERROR_MESSAGE);
        }
    }

    private void checkUserInvitation(User user, Photo photo) {
        if (photo.getContest().getContestType().getTypeName().equals(CONTEST_TYPE_INVITATION)) {
            if (!photo.getContest().getInvitedUsers().contains(user)) {
                throw new UnauthorizedOperationException(UNINVITED_USER_PHOTO_SUBMISSION_ERROR_MESSAGE);
            }
        }
    }

    private static void checkIfJury(Photo photo, User user) {
        Contest contest = photo.getContest();

        if (contest.getJury().contains(user)) {
            throw new UnauthorizedOperationException(JURY_PHOTO_SUBMISSION_ERROR_MESSAGE);
        }
    }

}
