package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.ContestCategory;
import java.util.List;

public interface ContestCategoryService {

    List<ContestCategory> getAll();

    ContestCategory getById(int id);

}
