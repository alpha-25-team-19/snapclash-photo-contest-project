package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.models.Ranking;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.RankingRepository;
import com.telerikacademy.photocontest.utils.AuthorizationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

@Service
public class RankingServiceImpl implements RankingService {

    private final RankingRepository rankingRepository;

    @Autowired
    public RankingServiceImpl(RankingRepository rankingRepository) {
        this.rankingRepository = rankingRepository;
    }

    @Override
    public List<Ranking> getAll(User user) {
        AuthorizationChecker.checkOrganiserAuthorization(user, ORGANISER_READ_AUTHORIZATION_ERROR_MESSAGE);
        return rankingRepository.getAll();
    }

    @Override
    public Ranking getByUser(User rankingUser, User user) {
        AuthorizationChecker.checkUserAuthorization(rankingUser.getId(), user, USER_READ_AUTHORIZATION_ERROR_MESSAGE);
        return rankingRepository.getByUser(rankingUser.getUsername());
    }

    @Override
    public void updateRanking(Ranking rankingToUpdate, User rankingUser, User user) {
        AuthorizationChecker.checkOrganiserAuthorization(user, UPDATE_USER_RANKING_AUTHORIZATION_ERROR_MESSAGE);
        checkDuplicateRanking(rankingToUpdate, rankingUser);

        rankingRepository.update(rankingToUpdate);
    }

    private void checkDuplicateRanking(Ranking rankingToUpdate, User rankingUser) {
        boolean duplicateExists = true;

        Ranking existingRanking = rankingRepository.getByUser(rankingUser.getUsername());
        if (existingRanking.getTotalScore() != rankingToUpdate.getTotalScore()) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(USER_RANKING_NO_CHANGE_DETECTED);
        }
    }
}
