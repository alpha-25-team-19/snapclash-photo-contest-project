package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;

import java.util.List;

public interface PhotoService {

    List<Photo> getAll();

    Photo getById(int id);

    List<Photo> filter(PhotoSearchParameters photoSearchParameters);

    void createPhoto(User user, Photo photo);
}
