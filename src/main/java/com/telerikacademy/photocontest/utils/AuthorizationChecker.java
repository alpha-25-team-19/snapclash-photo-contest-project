package com.telerikacademy.photocontest.utils;

import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.User;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

public class AuthorizationChecker {

    public static void checkOrganiserAuthorization(User user, String message) {
        if (!user.getRole().getRoleName().equals(ORGANISER_ROLE)) {
            throw new UnauthorizedOperationException(message);
        }
    }

    public static void checkUserAuthorization(int id, User user, String message) {
        if (!user.getRole().getRoleName().equals(ORGANISER_ROLE)
                && user.getId() != id) {
            throw new UnauthorizedOperationException(message);
        }
    }

}
