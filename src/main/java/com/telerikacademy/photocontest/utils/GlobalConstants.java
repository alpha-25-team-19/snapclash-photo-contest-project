package com.telerikacademy.photocontest.utils;

public class GlobalConstants {

    // Length / Size / Range validation
        // Messages
    public static final String USER_LAST_NAME_LENGTH_RANGE_MESSAGE = "User's last name should be between 2 and 50 symbols.";
    public static final String USER_FIRST_NAME_LENGTH_RANGE_MESSAGE = "User's first name should be between 2 and 50 symbols.";
    public static final String USER_PASSWORD_LENGTH_RANGE_MESSAGE = "User's password should be between 8 and 50 symbols.";
    public static final String USER_USERNAME_LENGTH_RANGE_MESSAGE = "User's username should be between 4 and 50 symbols.";
    public static final String CONTEST_TITLE_LENGTH_RANGE_MESSAGE = "The title should be between 2 and 50 symbols";
    public static final String START_DATE_PHASE_ONE_VALIDATION_MESSAGE = "Start of phase one, must be future date";
    public static final String END_DATE_PHASE_ONE_VALIDATION_MESSAGE = "End of phase one, must be future date";
    public static final String END_DATE_PHASE_TWO_VALIDATION_MESSAGE = "End of phase two, must be future date";
    public static final String PHASE_ONE_TIMESPAN_VALIDATION_ERROR_MESSAGE = "Submission phase should be between %s and %s days long";
    public static final String PHASE_TWO_TIMESPAN_VALIDATION_ERROR_MESSAGE = "Deliberation phase should be between %s and %s hours long";
    public static final String PHOTO_TITLE_LENGTH_RANGE_MESSAGE = "Photo title should be between 5 and 50 symbols";
    public static final String PHOTO_STORY_LENGTH_RANGE_MESSAGE = "Photo story should be between 20 and 500 symbols";
    public static final String PHOTO_SCORE_RANGE_MESSAGE = "Photo Score must be between 1 and 10";
    public static final String PHOTO_COMMENT_MAX_LENGTH_MESSAGE = "Photo Comment should be under 500 symbols";
    public static final String PHOTO_REVIEW_COMMENT_NOT_BLANK = "Photo review comment should not be blank";

        // Range Min-Max
    public static final int MIN_CONTEST_TITLE_LENGTH = 2;
    public static final int MAX_CONTEST_TITLE_LENGTH = 50;
    public static final int MIN_USER_USERNAME_LENGTH = 4;
    public static final int MAX_USER_USERNAME_LENGTH = 50;
    public static final int MIN_USER_NAME_LENGTH = 2;
    public static final int MAX_USER_NAME_LENGTH = 50;
    public static final int MIN_PASSWORD_LENGTH = 8;
    public static final int MAX_PASSWORD_LENGTH = 50;
    public static final int MIN_PHOTO_TITLE_LENGTH = 5;
    public static final int MAX_PHOTO_TITLE_LENGTH = 50;
    public static final int MIN_PHOTO_STORY_LENGTH = 20;
    public static final int MAX_PHOTO_STORY_LENGTH = 500;
    public static final int MIN_PHOTO_SCORE = 1;
    public static final int MAX_PHOTO_SCORE = 10;
    public static final int MAX_PHOTO_COMMENT_SIZE = 500;
    public static final int MIN_PHASE_ONE_DAYS = 1;
    public static final int MAX_PHASE_ONE_DAYS = 30;
    public static final int MIN_PHASE_TWO_HOURS = 1;
    public static final int MAX_PHASE_TWO_HOURS = 24;

    // ID validation
    public static final String JUROR_ID_POSITIVE = "Juror ID should be positive";
    public static final String PHOTO_ID_POSITIVE = "Photo ID should be positive";
    public static final String CONTEST_ID_POSITIVE = "Contest ID should be positive";

    // Authorization
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String WRONG_USERNAME_PASSWORD = "Wrong username/password.";
    public static final String NO_USER_LOGGED = "No logged in user.";

        // USER
    public static final String UNINVITED_USER_PHOTO_SUBMISSION_ERROR_MESSAGE = "Only invited users can submit photos to this contest";
    public static final String INVALID_USER_PHOTO_SUBMISSION_ERROR_MESSAGE = "You cannot submit photos on behalf of other users";
    public static final String INVALID_USER_PHOTO_UPLOAD_ERROR_MESSAGE = "You cannot upload photos on behalf of other users";
    public static final String USER_DUPLICATE_PHOTO_SUBMISSION_ERROR_MESSAGE = "You have already submitted a photo to this contest";
    public static final String USER_FILTER_AUTHORIZATION_ERROR_MESSAGE = "Users can only filter contests in which they are participating.";

        // ORGANISER
    public static final String ORGANISER_READ_AUTHORIZATION_ERROR_MESSAGE = "Only Organisers can view this information";
    public static final String ORGANISER_CREATE_CONTEST_AUTHORIZATION_ERROR_MESSAGE = "Only Organisers can create contests";
    public static final String USER_PROMOTION_AUTHORIZATION_ERROR_MESSAGE = "Only organisers can promote users to organisers";
    public static final String ORGANISER_CONTEST_AWARDS_AUTHORIZATION_ERROR_MESSAGE = "Only organisers can allocate points to winners from contests";
    public static final String UPDATE_USER_RANKING_AUTHORIZATION_ERROR_MESSAGE = "Only organisers can update a user's ranking";
    public static final String ORGANISER_SUBMISSION_ERROR_MESSAGE = "Organisers cannot submit photos to contests";

        // JURY
    public static final String JURY_PHOTO_SUBMISSION_ERROR_MESSAGE = "Jury members cannot submit photos";
    public static final String PHOTO_REVIEW_INVALID_JURY_MESSAGE = "%s is not a member of '%s' jury and cannot submit a photo review";
    public static final String INVALID_JURY_SUBMISSION_ERROR_MESSAGE = "You can not submit reviews on behalf of other users";

        // MIXED
    public static final String ORGANISER_CONTEST_JURY_AUTHORIZATION_ERROR_MESSAGE = "Only organisers, photo masters and photo dictators can be jury in contests";
    public static final String DELETE_REVIEW_AUTHORIZATION_ERROR_MESSAGE = "Photo reviews can only be deleted by organisers or the users who posted them";
    public static final String USER_READ_AUTHORIZATION_ERROR_MESSAGE = "User information can only be viewed by Organisers or the User themselves";
    public static final String USER_UPDATE_AUTHORIZATION_ERROR_MESSAGE = "User information can only be edited by Organisers or the User themselves";
    public static final String PARTICIPANT_CONTEST_AUTHORIZATION_ERROR_MESSAGE = "Organisers or jury cannot participate in contests";

    // Duplication
    public static final String USER_ALREADY_EXISTS = "User already exists";
    public static final String CONTEST_ALREADY_EXISTS = "Contest with this title already exists";
    public static final String REVIEW_ALREADY_EXISTS = "You have already submitted a review for this photo";
    public static final String CONTEST_POINTS_ALREADY_ALLOCATED = "The points for this contest have already been allocated";
    public static final String USER_RANKING_NO_CHANGE_DETECTED = "New user ranking score is same as old user ranking score";
    public static final String PHOTO_FILE_ALREADY_UPLOADED = "A file has already been uploaded for this photo";

    //Invalid phase
    public static final String CONTEST_FINISHED_PHASE_ERROR_MESSAGE = "The contest must be in phase 'finished' to allocate points.";
    public static final String CONTEST_PHASE_TWO_ERROR_MESSAGE = "Reviews can only be submitted when a contest is in phase 'two'.";
    public static final String CONTEST_PHASE_ONE_DATE_VALIDATION_ERROR_MESSAGE = "The start of phase one must be before the end of phase one";
    public static final String CONTEST_PHASE_TWO_DATE_VALIDATION_ERROR_MESSAGE = "The start of phase two must be before the end of phase two";

    // Scores
    public static final int INVALID_CATEGORY_PHOTO_SCORE = 0;
    public static final int FIRST_PLACE_DOUBLE_SCORE = 75;
    public static final int FIRST_PLACE_SINGLE_SCORE = 50;
    public static final int FIRST_PLACE_SHARED_SCORE = 40;
    public static final int SECOND_PLACE_SINGLE_SCORE = 35;
    public static final int SECOND_PLACE_SHARED_SCORE = 25;
    public static final int THIRD_PLACE_SINGLE_SCORE = 20;
    public static final int THIRD_PLACE_SHARED_SCORE = 10;
    public static final int PHOTO_ENTHUSIAST_MIN_SCORE = 51;
    public static final int PHOTO_MASTER_MIN_SCORE = 151;
    public static final int PHOTO_DICTATOR_MIN_SCORE = 1001;

    // Roles / Types
    public static final String ORGANISER_ROLE = "Organiser";
    public static final String USER_ROLE = "User";
    public static final String CONTEST_TYPE_INVITATION = "Invitational";
    public static final String CONTEST_TYPE_OPEN = "Open";
    public static final String CONTEST_PHASE_FINISHED = "Finished";
    public static final String CONTEST_PHASE_TWO = "Phase two";

    // Contest Time
    public static final String CONTEST_SUBMISSION_INVALID_PHASE = "You cannot submit photos to this contest at this stage";

    // Defaults
    public static final String DEFAULT_PHOTO_REVIEW_COMMENT = "Assigned Default Score, since Juror did not submit a review in time";
    public static final String DEFAULT_USERNAME = "default.username";
    public static final int DEFAULT_PHOTO_REVIEW_SCORE = 3;
    public static final int DEFAULT_POINTS_FOR_INVITATIONAL_CONTEST = 3;
    public static final int DEFAULT_POINTS_FOR_OPEN_CONTEST = 1;
    public static final int DEFAULT_USER_SCORE = 0;


    // Other
    public static final String CREATE_DIRECTORY_ERROR_MESSAGE = "Could not create the directory where the uploaded files will be stored.";
    public static final String FILE_STORAGE_ERROR_MESSAGE = "Could not store file %s Please try again!";

    // Not Found
    public static final String NO_RESULTS_FOUND = "No results found";

    // Contest Phases
    public static final String NOT_STARTED = "not-started";
    public static final String PHASE_ONE = "phase-one";
    public static final String PHASE_TWO = "phase-two";
    public static final String NOT_FINISHED = "not-finished";
    public static final String FINISHED = "finished";
    public static final String INVALID_CONTEST_PHASE = "%s is not a valid contest phase";

    // User Rankings
    public static final String PHOTO_JUNKIE = "photo-junkie";
    public static final String PHOTO_ENTHUSIAST = "photo-enthusiast";
    public static final String PHOTO_MASTER = "photo-master";
    public static final String PHOTO_DICTATOR = "photo-dictator";
    public static final String INVALID_USER_RANKING = "Ranking %s does not exist";
    public static final String MAX_USER_RANKING = "Max rank achieved";

}
