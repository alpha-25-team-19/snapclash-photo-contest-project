package com.telerikacademy.photocontest;

import com.telerikacademy.photocontest.models.properties.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class SnapClashApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnapClashApplication.class, args);
    }

}
