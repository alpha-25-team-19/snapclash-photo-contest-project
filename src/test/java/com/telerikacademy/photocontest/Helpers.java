package com.telerikacademy.photocontest;

import com.telerikacademy.photocontest.models.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static com.telerikacademy.photocontest.utils.GlobalConstants.*;

public class Helpers {

    public static Ranking createMockRanking(){
        var mockRanking = new Ranking();
        mockRanking.setId(1);
        mockRanking.setTotalScore(DEFAULT_USER_SCORE);
        mockRanking.setUser(createMockUser());
        return mockRanking;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirstName("Mock");
        mockUser.setLastName("User");
        mockUser.setUsername("mock.user");
        mockUser.setPassword("mock.user");
        mockUser.setRole(createMockRoleUser());
        return mockUser;
    }

    public static Role createMockRoleUser() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRoleName(USER_ROLE);
        return mockRole;
    }

    public static User createMockOrganiser() {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setFirstName("Mock");
        mockUser.setLastName("User");
        mockUser.setUsername("mock.organiser");
        mockUser.setPassword("mock.user");
        mockUser.setRole(createMockRoleOrganiser());
        return mockUser;
    }

    public static Role createMockRoleOrganiser() {
        var mockRole = new Role();
        mockRole.setId(2);
        mockRole.setRoleName(ORGANISER_ROLE);
        return mockRole;
    }

    public static Photo createMockPhoto() {
        var mockPhoto = new Photo();
        mockPhoto.setId(1);
        mockPhoto.setTitle("Mock Photo");
        mockPhoto.setStory("Mock Photo Story");
        mockPhoto.setPhotoUser(createMockUser());
        mockPhoto.setContest(createOpenMockContest());

        return mockPhoto;
    }

    public static Contest createOpenMockContest() {
        var mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("Mock Contest");
        mockContest.setContestCategory(createMockContestCategory());
        mockContest.setContestType(createMockOpenContestType());

        LocalDateTime firstPhaseEnd = LocalDateTime.now().plusDays(7);

        mockContest.setPhaseOneStart(LocalDateTime.now().minusDays(1));
        mockContest.setPhaseTwoStart(firstPhaseEnd);
        mockContest.setContestEnd(firstPhaseEnd.plusHours(23));

        Set<User> mockJury = new HashSet<>();
        mockJury.add(createMockOrganiser());

        mockContest.setJury(mockJury);

        return mockContest;
    }

    public static Contest createMockInvitationalContest() {
        var mockContest = createOpenMockContest();
        mockContest.setContestType(createMockInvitationalContestType());

        Set<User> mockInvitedUsers = new HashSet<>();
        mockContest.setInvitedUsers(mockInvitedUsers);

        return mockContest;
    }

    public static ContestCategory createMockContestCategory() {
        var mockCategory = new ContestCategory();
        mockCategory.setId(1);
        mockCategory.setCategoryName("Animals");

        return mockCategory;
    }

    public static ContestType createMockOpenContestType() {
        var mockType = new ContestType();
        mockType.setId(1);
        mockType.setTypeName(CONTEST_TYPE_OPEN);

        return mockType;
    }

    public static ContestType createMockInvitationalContestType() {
        var mockType = new ContestType();
        mockType.setId(2);
        mockType.setTypeName(CONTEST_TYPE_INVITATION);

        return mockType;
    }

    public static PhotoReview createMockPhotoReview(){
        var mockReview = new PhotoReview();
        mockReview.setId(1);
        mockReview.setJuror(createMockOrganiser());
        mockReview.setPhoto(createMockPhoto());
        mockReview.setPhotoScore(DEFAULT_PHOTO_REVIEW_SCORE);
        mockReview.setPhotoComment(DEFAULT_PHOTO_REVIEW_COMMENT);
        mockReview.setInvalidCategory(false);

        return mockReview;
    }

    public static ContestLeaderboard createMockLeaderboard(){
        var mockLeaderboard = new ContestLeaderboard();
        mockLeaderboard.setContest(createOpenMockContest());
        mockLeaderboard.setFirstPlaceWinners(new HashMap<>());
        mockLeaderboard.setSecondPlaceWinners(new HashMap<>());
        mockLeaderboard.setThirdPlaceWinners(new HashMap<>());

        return mockLeaderboard;
    }

    public static Contest createMockContestPhaseTwo() {
        Contest mockContest = createOpenMockContest();
        mockContest.setPhaseOneStart(LocalDateTime.now().minusDays(3));
        mockContest.setPhaseTwoStart(LocalDateTime.now().minusHours(3));
        mockContest.setContestEnd(LocalDateTime.now().plusHours(7));

        return mockContest;
    }

    public static Contest createMockContestFinished() {
        Contest mockContest = createOpenMockContest();
        mockContest.setPhaseOneStart(LocalDateTime.now().minusDays(3));
        mockContest.setPhaseTwoStart(LocalDateTime.now().minusHours(3));
        mockContest.setContestEnd(LocalDateTime.now().minusHours(1));

        return mockContest;
    }
}
