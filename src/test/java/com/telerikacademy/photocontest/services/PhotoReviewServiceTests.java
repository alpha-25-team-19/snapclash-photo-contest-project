package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.InvalidContestPhaseException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.PhotoReview;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.PhotoReviewRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.List;

import static com.telerikacademy.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhotoReviewServiceTests {

    @Mock
    PhotoReviewRepository mockPhotoReviewRepository;

    @InjectMocks
    PhotoReviewServiceImpl photoReviewService;


    @Test
    public void getAllPhotoReviews_Should_Call_Repository() {
        photoReviewService.getAll();

        Mockito.verify(mockPhotoReviewRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByPhoto_Should_Call_Repository() {
        photoReviewService.getByPhoto(Mockito.anyInt());

        Mockito.verify(mockPhotoReviewRepository, Mockito.times(1)).getByPhoto(Mockito.anyInt());
    }


    @Test
    public void getById_Should_Call_Repository() {
        photoReviewService.getById(Mockito.anyInt());

        Mockito.verify(mockPhotoReviewRepository, Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void createPhotoReview_Should_Throw_When_Wrong_Contest_Phase() {
        PhotoReview mockReview = createMockPhotoReview();

        Assertions.assertThrows(InvalidContestPhaseException.class,
                () -> photoReviewService.createPhotoReview(mockReview, createMockUser()));
    }

    @Test
    public void createPhotoReview_Should_Throw_When_User_Not_Juror() {
        PhotoReview mockReview = createMockPhotoReview();
        mockReview.getPhoto().setContest(createMockContestPhaseTwo());

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoReviewService.createPhotoReview(mockReview, createMockUser()));
    }

    @Test
    public void createPhotoReview_Should_Throw_When_User_Not_On_Jury() {
        PhotoReview mockReview = createMockPhotoReview();
        mockReview.getPhoto().setContest(createMockContestPhaseTwo());
        User mockUser = createMockUser();
        mockReview.setJuror(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoReviewService.createPhotoReview(mockReview, mockUser));
    }

    @Test
    public void createPhotoReview_Should_Throw_When_Multiple_Reviews_Of_Same_Photo() {
        PhotoReview mockReview = createMockPhotoReview();
        mockReview.getPhoto().setContest(createMockContestPhaseTwo());
        User mockUser = createMockOrganiser();

        Mockito.when(mockPhotoReviewRepository.getByJuror(Mockito.anyInt())).thenReturn(List.of(mockReview));

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> photoReviewService.createPhotoReview(mockReview, mockUser));
    }

    @Test
    public void createPhotoReview_Should_Create_Valid_Review() {
        PhotoReview mockReview = createMockPhotoReview();
        mockReview.getPhoto().setContest(createMockContestPhaseTwo());
        User mockUser = createMockOrganiser();

        photoReviewService.createPhotoReview(mockReview, mockUser);

        Mockito.verify(mockPhotoReviewRepository, Mockito.times(1)).create(mockReview);
    }

    @Test
    public void getPhotoFinalScore_Should_Throw_When_Wrong_Contest_Phase() {
        Photo mockPhoto = createMockPhoto();
        mockPhoto.setContest(createMockContestPhaseTwo());

        Assertions.assertThrows(InvalidContestPhaseException.class,
                () -> photoReviewService.getPhotoFinalScore(mockPhoto));
    }

    @Test
    public void getPhotoFinalScore_Should_Generate_Default_Reviews_When_Not_All_Jurors_Submitted() {
        Photo mockPhoto = createMockPhoto();
        mockPhoto.setContest(createMockContestFinished());
        photoReviewService.getPhotoFinalScore(mockPhoto);

        Mockito.verify(mockPhotoReviewRepository, Mockito.times(1)).create(Mockito.any(PhotoReview.class));
    }

    @Test
    public void getPhotoFinalScore_Should_Call_Repository() {
        Photo mockPhoto = createMockPhoto();
        mockPhoto.setContest(createMockContestFinished());
        photoReviewService.getPhotoFinalScore(mockPhoto);

        Mockito.verify(mockPhotoReviewRepository, Mockito.times(1)).getPhotoFinalScore(Mockito.anyInt());
    }



}
