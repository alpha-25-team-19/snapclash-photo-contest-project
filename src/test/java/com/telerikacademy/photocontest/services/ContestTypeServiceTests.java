package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.repositories.ContestTypeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
@ExtendWith(MockitoExtension.class)
public class ContestTypeServiceTests {

    @Mock
    ContestTypeRepository contestTypeRepository;

    @InjectMocks
    ContestTypeServiceImpl contestTypeService;

    @Test
    public void getAllTypes_Should_Call_Repository() {
        contestTypeService.getAll();

        Mockito.verify(contestTypeRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByID_Should_Call_Repository() {
        contestTypeService.getById(Mockito.anyInt());

        Mockito.verify(contestTypeRepository, Mockito.times(1)).getById(Mockito.anyInt());
    }

}
