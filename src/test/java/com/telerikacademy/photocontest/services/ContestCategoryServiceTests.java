package com.telerikacademy.photocontest.services;


import com.telerikacademy.photocontest.repositories.ContestCategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ContestCategoryServiceTests {


    @Mock
    ContestCategoryRepository contestCategoryRepository;

    @InjectMocks
    ContestCategoryServiceImpl contestCategoryService;

    @Test
    public void getAllCategories_Should_Call_Repository() {
        contestCategoryService.getAll();

        Mockito.verify(contestCategoryRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByID_Should_Call_Repository() {
        contestCategoryService.getById(Mockito.anyInt());

        Mockito.verify(contestCategoryRepository, Mockito.times(1)).getById(Mockito.anyInt());
    }
}
