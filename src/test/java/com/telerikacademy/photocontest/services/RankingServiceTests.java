package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.repositories.RankingRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class RankingServiceTests {

    @Mock
    RankingRepository mockRankingRepository;

    @InjectMocks
    RankingServiceImpl rankingService;

    @Test
    public void getAllRankings_Should_Call_Repository() {
        var mockOrganiser = createMockOrganiser();
        rankingService.getAll(mockOrganiser);

        Mockito.verify(mockRankingRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAllRankings_Should_Throw_When_UserIsNotOrganiser() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> rankingService.getAll(mockUser));
    }

    @Test
    public void getByUser_Should_Call_Repository() {
        var mockOrganiser = createMockOrganiser();
        var mockUser = createMockUser();
        rankingService.getByUser(mockUser, mockOrganiser);

        Mockito.verify(mockRankingRepository, Mockito.times(1)).getByUser(mockUser.getUsername());
    }

    @Test
    public void getByUser_Should_Throw_When_WrongUserAuthorization() {
        var mockUser = createMockUser();
        var mockRankingUser = createMockUser();
        mockRankingUser.setId(3);

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> rankingService.getByUser(mockRankingUser, mockUser));
    }

    @Test
    public void updateRanking_Should_Call_Repository() {
        var mockOrganiser = createMockOrganiser();
        var mockRankingUser = createMockUser();
        var mockRanking = createMockRanking();
        mockRanking.setTotalScore(50);

        Mockito.when(mockRankingRepository.getByUser(Mockito.anyString())).thenReturn(createMockRanking());

        rankingService.updateRanking(mockRanking, mockRankingUser, mockOrganiser);

        Mockito.verify(mockRankingRepository, Mockito.times(1)).update(mockRanking);
    }

    @Test
    public void updateRanking_Should_Throw_When_UserIsNotOrganiser() {
        var mockUser = createMockUser();
        var mockRankingUser = createMockUser();
        var mockRanking = createMockRanking();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> rankingService.updateRanking(mockRanking, mockRankingUser, mockUser));
    }

    @Test
    public void updateRanking_Should_Throw_When_DuplicateScoreIsPassed() {
        var mockUser = createMockOrganiser();
        var mockRankingUser = createMockUser();
        var mockRanking = createMockRanking();

        Mockito.when(mockRankingRepository.getByUser(Mockito.anyString())).thenReturn(createMockRanking());

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> rankingService.updateRanking(mockRanking, mockRankingUser, mockUser));
    }

}
