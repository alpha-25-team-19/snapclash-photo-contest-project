package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.search.UserSearchParameters;
import com.telerikacademy.photocontest.repositories.RankingRepository;
import com.telerikacademy.photocontest.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import static com.telerikacademy.photocontest.Helpers.*;
import static com.telerikacademy.photocontest.utils.GlobalConstants.DEFAULT_USERNAME;


@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @Mock
    RankingRepository rankingRepository;

    @InjectMocks
    UserServiceImpl userService;

    private UserSearchParameters userSearchParameter;

    @Test
    public void getAllUsers_Should_Call_Repository() {
        var mockOrganiser = createMockOrganiser();
        userService.getAll(mockOrganiser);

        Mockito.verify(userRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByUsername_Should_Call_Repository() {
        userService.getByUsername(Mockito.anyString());

        Mockito.verify(userRepository, Mockito.times(1)).getByUsername(Mockito.anyString());
    }

    @Test
    public void getByID_Should_Call_Repository() {
        var mockOrganiser = createMockOrganiser();

        userService.getById(Mockito.anyInt(), mockOrganiser);

        Mockito.verify(userRepository, Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void getById_Should_Throw_When_UserIsNotOrganiser() {
        var mockUser = createMockUser();
        var mockUser2 = createMockUser();
        mockUser2.setId(2);
        mockUser2.setUsername(DEFAULT_USERNAME);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.getById(1, mockUser2));
    }

    @Test
    public void getAllEligibleJurors_Should_Call_Repository() {
        userService.getAllEligibleJurors();

        Mockito.verify(userRepository, Mockito.times(1)).getAllEligibleJurors();
        Mockito.verify(userRepository, Mockito.times(1)).getAllOrganisers();
    }

    @Test
    public void createUser_Should_Throw_When_DuplicateExists() {
        var mockUser = createMockUser();
        var mockUser2 = createMockUser();

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.createUser(mockUser2, createMockRanking()));
    }

    @Test
    public void createUser_Should_CallRepository_When_ValidUserIsPassed() {
        var mockUser = createMockUser();
        var mockRanking = createMockRanking();

        Mockito.when(userRepository.getByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        userService.createUser(mockUser, mockRanking);

        Mockito.verify(userRepository, Mockito.times(1)).create(mockUser);
        Mockito.verify(rankingRepository, Mockito.times(1)).create(mockRanking);
    }

    @Test
    public void update_Should_Throw_When_UserIsUpdatingOtherUsersProfile() {
        var mockUser = createMockUser();
        var mockUser2 = createMockUser();
        mockUser2.setId(2);
        mockUser2.setUsername(DEFAULT_USERNAME);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.update(mockUser2, mockUser));
    }

    @Test
    public void update_Should_Throw_When_UserDoesNotPassPromotionAuthorization(){
        var userToUpdate = createMockOrganiser();
        var user = createMockUser();


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.update(userToUpdate, user));
    }


    @Test
    public void updateUser_Should_CallRepository_When_ValidParametersArePassed() {
        var mockUser = createMockUser();
        var organiser = createMockOrganiser();
        mockUser.setId(11);
        mockUser.setUsername(DEFAULT_USERNAME);

        var mockRanking = createMockRanking();
        mockRanking.setUser(mockUser);

        Mockito.when(userRepository.getByUsername(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);
        userService.update(mockUser, organiser);

        Mockito.verify(userRepository, Mockito.times(1)).update(mockUser);

    }

    @Test
    public void updateUser_Should_CallRepository_When_ewValidParametersArePassed() {
        var mockUser = createMockUser();
        var organiser = createMockOrganiser();
        mockUser.setId(11);
        mockUser.setUsername(DEFAULT_USERNAME);
        mockUser.setRole(createMockRoleOrganiser());

        var mockRanking = createMockRanking();
        mockRanking.setUser(mockUser);

        Mockito.when(userRepository.getByUsername(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);
        userService.update(mockUser, organiser);

        Mockito.verify(userRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    public void filter_Should_Throw_When_UserIsNotOrganiser(){
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.filter(userSearchParameter, mockUser));
    }

    @Test
    public void filter_Should_Call_Repository(){
        var mockOrganiser = createMockOrganiser();

        userService.filter(userSearchParameter, mockOrganiser);

        Mockito.verify(userRepository, Mockito.times(1)).filter(userSearchParameter);
    }

}
