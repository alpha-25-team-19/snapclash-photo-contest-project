package com.telerikacademy.photocontest.services;



import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.EntityNotFoundException;
import com.telerikacademy.photocontest.exceptions.InvalidPeriodException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.ContestLeaderboard;
import com.telerikacademy.photocontest.models.search.ContestSearchParameters;
import com.telerikacademy.photocontest.repositories.ContestRepository;
import com.telerikacademy.photocontest.repositories.RankingRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static com.telerikacademy.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceTests {


    @Mock
    ContestRepository contestRepository;

    @Mock
    RankingRepository rankingRepository;

    @InjectMocks
    ContestServiceImpl contestService;

    @Test
    public void getAllContests_Should_Call_Repository() {
        contestService.getAll();

        Mockito.verify(contestRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByID_Should_Call_Repository() {
        contestService.getById(Mockito.anyInt());

        Mockito.verify(contestRepository, Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void filter_Should_Throw_When_UserIsNotOrganiser() {
        var mockUser = createMockUser();

        ContestSearchParameters contestSearchParameters = new ContestSearchParameters(
                null, null, null, null, 4);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.filter(contestSearchParameters, mockUser));
    }

    @Test
    public void filter_Should_Call_Repository(){
        var mockUser = createMockUser();

        ContestSearchParameters contestSearchParameters = new ContestSearchParameters(
                null, null, null, null, mockUser.getId());
        contestService.filter(contestSearchParameters, mockUser);

        Mockito.verify(contestRepository, Mockito.times(1)).filter(contestSearchParameters);
    }


    @Test
    public void createContest_Should_Throw_When_UserIsNotOrganiser() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.createContest(mockUser, createOpenMockContest()));
    }

    @Test
    public void createContest_Should_Throw_When_DuplicateExists() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();
        var mockContest2 = createOpenMockContest();

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> contestService.createContest(mockOrganiser, mockContest2));
    }

    @Test
    public void createContest_Should_Throw_When_PhaseOneStartIsAfterPhaseOneEnd() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();
        mockContest.setPhaseTwoStart(LocalDateTime.now());
        mockContest.setPhaseOneStart(LocalDateTime.now().plusHours(4));

        Mockito.when(contestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(InvalidPeriodException.class,
                () -> contestService.createContest(mockOrganiser, mockContest));
    }


    @Test
    public void createContest_Should_Throw_When_PhaseOneEndIsMoreThanMonthAfterPhaseOneStart() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();
        mockContest.setPhaseTwoStart(LocalDateTime.now());
        mockContest.setPhaseTwoStart(LocalDateTime.now().plusMonths(2));

        Mockito.when(contestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(InvalidPeriodException.class,
                () -> contestService.createContest(mockOrganiser, mockContest));
    }

    @Test
    public void createContest_Should_Throw_When_PhaseOneEndIsLessThanDayAfterPhaseOneStart() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();
        mockContest.setPhaseOneStart(LocalDateTime.now());
        mockContest.setPhaseTwoStart(LocalDateTime.now().plusHours(4));

        Mockito.when(contestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(InvalidPeriodException.class,
                () -> contestService.createContest(mockOrganiser, mockContest));
    }

    @Test
    public void createContest_Should_Throw_When_PhaseTwoStartIsAfterPhaseTwoEnd() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();
        mockContest.setContestEnd(LocalDateTime.now());
        mockContest.setPhaseTwoStart(LocalDateTime.now().plusHours(4));

        Mockito.when(contestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(InvalidPeriodException.class,
                () -> contestService.createContest(mockOrganiser, mockContest));
    }

    @Test
    public void createContest_Should_Throw_When_PhaseTwoEndIsMLessThanHourAfterPhaseOneStart() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();
        mockContest.setPhaseTwoStart(LocalDateTime.now());
        mockContest.setContestEnd(LocalDateTime.now().plusMinutes(30));

        Mockito.when(contestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(InvalidPeriodException.class,
                () -> contestService.createContest(mockOrganiser, mockContest));
    }


    @Test
    public void createContest_Should_Throw_When_InvalidJuryIsPassed() {
        var mockUser = createMockUser();
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();
        var mockRanking = createMockRanking();
        mockContest.getJury().add(mockUser);

        Mockito.when(contestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(rankingRepository.getByUser(mockUser.getUsername())).thenReturn(mockRanking);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.createContest(mockOrganiser, mockContest));
    }

    @Test
    public void createContest_Should_Throw_When_InvalidUsersAreInvited() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createMockInvitationalContest();
        mockContest.getInvitedUsers().add(mockOrganiser);

        Mockito.when(contestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.createContest(mockOrganiser, mockContest));
    }

    @Test
    public void createContest_Should_Call_Repository_When_ValidContestIsPassed() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();

        Mockito.when(contestRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);
        contestService.createContest(mockOrganiser, mockContest);

        Mockito.verify(contestRepository, Mockito.times(1)).create(mockContest);
    }

    @Test
    public void allocateContestPoints_Should_Throw_When_UserIsNotOrganiser() {
        var mockUser = createMockUser();
        ContestLeaderboard contestLeaderboard = new ContestLeaderboard();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.allocateContestPoints(contestLeaderboard, mockUser));
    }

    @Test
    public void allocateContestPoints_Should_Throw_When_ContestAlreadyAwarded() {
        var mockOrganiser = createMockOrganiser();
        var mockContest = createOpenMockContest();
        ContestLeaderboard contestLeaderboard = new ContestLeaderboard();
        mockContest.setAwarded(true);
        contestLeaderboard.setContest(mockContest);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> contestService.allocateContestPoints(contestLeaderboard, mockOrganiser));
    }

    @Test
    public void allocateContestPoints_Should_Call_Repository_When_ValidParametersArePassed() {
        var mockOrganiser = createMockOrganiser();
        var mockUser = createMockUser();
        var mockContest = createOpenMockContest();
        var mockRanking = createMockRanking();
        var contestLeaderboard = createMockLeaderboard();
        contestLeaderboard.setContest(mockContest);
        contestLeaderboard.getFirstPlaceWinners().put(mockUser.getUsername(), 10.0);
        contestLeaderboard.getSecondPlaceWinners().put(mockUser.getUsername(), 5.0);
        contestLeaderboard.getThirdPlaceWinners().put(mockUser.getUsername(), 3.0);

        Mockito.when(rankingRepository.getByUser(mockUser.getUsername())).thenReturn(mockRanking);
        contestService.allocateContestPoints(contestLeaderboard, mockOrganiser);

        Mockito.verify(contestRepository, Mockito.times(1)).update(contestLeaderboard.getContest());
    }

    @Test
    public void allocateContestPoints_Should_Call_Repository_When_ValidParametersArePassed2() {
        var mockOrganiser = createMockOrganiser();
        var mockUser = createMockUser();
        var mockUser2 = createMockUser();
        mockUser2.setUsername("mockusername2");
        var mockContest = createOpenMockContest();
        var mockRanking = createMockRanking();
        var contestLeaderboard = createMockLeaderboard();
        contestLeaderboard.setContest(mockContest);
        contestLeaderboard.getFirstPlaceWinners().put(mockUser.getUsername(), 10.0);
        contestLeaderboard.getFirstPlaceWinners().put(mockUser2.getUsername(), 10.0);
        contestLeaderboard.getSecondPlaceWinners().put(mockUser.getUsername(), 5.0);
        contestLeaderboard.getThirdPlaceWinners().put(mockUser.getUsername(), 3.0);

        Mockito.when(rankingRepository.getByUser(mockUser.getUsername())).thenReturn(mockRanking);
        Mockito.when(rankingRepository.getByUser(mockUser2.getUsername())).thenReturn(mockRanking);
        contestService.allocateContestPoints(contestLeaderboard, mockOrganiser);

        Mockito.verify(contestRepository, Mockito.times(1)).update(contestLeaderboard.getContest());
    }

}
