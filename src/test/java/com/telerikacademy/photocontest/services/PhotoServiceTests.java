package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Photo;
import com.telerikacademy.photocontest.models.search.PhotoSearchParameters;
import com.telerikacademy.photocontest.repositories.PhotoRepository;
import com.telerikacademy.photocontest.repositories.RankingRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.*;

import static com.telerikacademy.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceTests {

    @Mock
    PhotoRepository mockPhotoRepository;

    @Mock
    RankingRepository mockRankingRepository;

    @InjectMocks
    PhotoServiceImpl photoService;

    @Test
    public void getAllPhotos_Should_Call_Repository() {
        photoService.getAll();

        Mockito.verify(mockPhotoRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_Call_Repository() {
        photoService.getById(Mockito.anyInt());

        Mockito.verify(mockPhotoRepository, Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void filter_Should_Call_Repository() {
        var mockSearchParameters = Mockito.any(PhotoSearchParameters.class);
        photoService.filter(mockSearchParameters);

        Mockito.verify(mockPhotoRepository, Mockito.times(1)).filter(mockSearchParameters);
    }

    @Test
    public void createPhoto_Should_Throw_When_UserNotSameAsPhotoUser() {
        var mockPhotoUser = createMockUser();
        mockPhotoUser.setUsername("MockUser2");

        var mockPhoto = createMockPhoto();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.createPhoto(mockPhotoUser, mockPhoto));

    }

    @Test
    public void createPhoto_Should_Throw_When_UserIsOrganiser() {
        var mockOrganiser = createMockOrganiser();
        var mockPhoto = createMockPhoto();
        mockPhoto.setPhotoUser(mockOrganiser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.createPhoto(mockOrganiser, mockPhoto));
    }

    @Test
    public void createPhoto_Should_Throw_When_ContestIsNotInPhaseOne() {
        var mockUser = createMockUser();
        var mockPhoto = createMockPhoto();
        mockPhoto.getContest().setPhaseOneStart(LocalDateTime.now().plusDays(1));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.createPhoto(mockUser, mockPhoto));
    }

    @Test
    public void createPhoto_Should_Throw_When_PhotoUserIsJury() {
        var mockUser = createMockUser();
        var mockPhoto = createMockPhoto();
        mockPhoto.getContest().getJury().add(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.createPhoto(mockUser, mockPhoto));
    }

    @Test
    public void createPhoto_Should_Throw_When_UserNotInvited() {
        var mockUser = createMockUser();
        var mockPhoto = createMockPhoto();
        mockPhoto.setContest(createMockInvitationalContest());

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.createPhoto(mockUser, mockPhoto));
    }

    @Test
    public void createPhoto_Should_Throw_When_UserSubmitsMoreThanOnePhoto() {
        var mockUser = createMockUser();
        var mockPhoto = createMockPhoto();
        var mockPhoto2 = createMockPhoto();
        mockPhoto2.setId(2);
        List<Photo> mockList = new ArrayList<>();
        mockList.add(mockPhoto);

        Mockito.when(mockPhotoRepository.filter(Mockito.any(PhotoSearchParameters.class))).thenReturn(mockList);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.createPhoto(mockUser, mockPhoto2));
    }

    @Test
    public void createPhoto_Should_Call_RepositoryOpenContest() {
        var mockUser = createMockUser();
        var mockPhoto = createMockPhoto();

        Mockito.when(mockRankingRepository.getByUser(Mockito.anyString())).thenReturn(createMockRanking());
        photoService.createPhoto(mockUser, mockPhoto);

        Mockito.verify(mockPhotoRepository, Mockito.times(1)).create(mockPhoto);
    }

    @Test
    public void createPhoto_Should_Call_RepositoryInvitationalContest() {
        var mockUser = createMockUser();
        var mockPhoto = createMockPhoto();
        var mockContest = createMockInvitationalContest();
        mockContest.getInvitedUsers().add(mockUser);
        mockPhoto.setContest(mockContest);

        Mockito.when(mockRankingRepository.getByUser(Mockito.anyString())).thenReturn(createMockRanking());
        photoService.createPhoto(mockUser, mockPhoto);

        Mockito.verify(mockPhotoRepository, Mockito.times(1)).create(mockPhoto);
    }



}
