<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# SnapClash - Photo Contest Project

### Stanyo Zhelev & Daniel Nikolov

****

### [Project Trello](https://trello.com/b/IZ4DlQ8x/snapclash-photo-contest-project)

### [Swagger Documentation](http://localhost:8080/swagger-ui.html) 

#### (available only while project is running)

### Database Relations:

![alt-text][database]

[database]: SnapClash%20Database.png

****

## Overview

- A team of aspiring photographers want an application that can allow them to easily manage online photo contests. The
  application has two main parts:

    - **Organisational** - here, the application owners (`Organisers`) can organize photo `Contests`.
    - **For Photo Junkies** - everyone is welcome to register and to participate in contests.
      `Junkies` with certain ranking can be invited to be `Jury`.

### Legend

- Must (🟥)
- Should (🟨)
- Could (🟩)

****

### Public Part

- The public part of your projects should be ***visible without authentication.***


- **Landing Page** (🟥)
    - You can show the latest winning photos or something else that might be compelling for people to register.


- **Login Form** (🟥)
    - Redirects to the private area of the application.
    - Requires `username` and `password`.


- **Register Form** (🟥)
    - Registers someone as a `Photo Junkie`.
    - Requires `username`, `password`, `first name` and `last name`.

****

### Private Part

- **Dashboard Page** (🟥)
    - Different for `Organisers` and `Photo Junkies`.
        - ***Organisers***:
            - A way to set up a new `Contest` (🟥)
            - A way to view `Contests` which are in `Phase I` (🟥)
            - A way to view `Contests` which are in `Phase II` (🟥)
            - A way to view `Contests` which are `Finished` (🟥)
            - A way to view `Photo Junkies` (🟥)
                - (*if scoring is implemented*) ordered by `Ranking` (🟨)
        - ***Photo Junkies***:
            - A way to view `Contests` which are `Active / Open` (🟥)
            - A way to view contests the `Photo Junkie` is currently participating in. (🟥)
            - A way to view `Contests` which are `Finished` and that the `Photo Junkie`
              has participated in (🟨)
            - (*if scoring is implemented*) Display `Current points` and `Ranking` and how many points until the next
              ranking is achieved (🟨)


- **Contest Page** (🟥)
    - The Contest Category is ***always visible***.
    - `Phase I` (🟥)
        - Remaining time until `Phase II` should be displayed.
        - `Jury` can view submitted `Photos` but ***cannot rate them yet***.
        - `Photo Junkies` see ***enroll button*** if the `Contest` is `Active / Open` and they are not participating.
        - If `Photo Junkies` are participating and have not uploaded a `Photo`, they see a form for upload:
            - `Photo Title` (🟥)
            - `Photo Story` (🟥)
                - Long text, which tells the captivating story of the `Photo`
            - `Photo` - File (🟥)
        - Only ***one*** `Photo` can be uploaded per participant.
            - The `Photo Title`, `Photo Story` and `Photo` itself ***cannot*** be edited. (🟥)
            - Display a ***warning*** on submit that any data cannot be changed later. (🟨)
    - `Phase II` (🟥)
        - Remaining time until `Finished`.
        - Participants ***cannot*** upload anymore.
        - `Jury` sees a form for each submitted `Photo`.
            - `Score [1-10]` (🟥)
            - `Comment` (🟥)
                - Long text
            - ***Checkbox*** to mark that the `Photo` does ***not*** fit the `Contest` `Category`.
                - If the checkbox is selected, **Score 0** is assigned automatically and a `Comment` that the
                  `Category` is wrong. This is the only way to assign a `Score` outside of [1-10] interval.
            - Each `Juror` can give ***one*** review per `Photo`.
            - If a `Photo` is not reviewed, a ***default*** `Score` of 3 is awarded.
    - `Finished` (🟥)
        - `Jury` can no longer review `Photos` (🟥)
        - Participants view their own `Score` and `Comments` (🟥)
        - Participants can also view the `Photos` submitted by other users, as well as their
          `Score` and `Comments` (🟨)


- **Create Contest Form** (🟥)
    - Either a new page, or on the organizer's dashboard.
    - The following must be easy to set up:
        - `Contest Title`
            - Text Field (🟥 - and unique)
        - `Contest Category`
            - Select (🟥)
        - `Active / Open` (🟥) or `Invitational` (🟨) `Contest`
            - `Active / Open` means that everyone (except the `Jury`) can join
            - `Invitational` - a list of users should be available, with the option to select / invite them. (🟨)
        - `Time Limit` `Phase I` (🟥)
            - Anything from ***one day*** to ***one month***
        - `Time Limit` `Phase II` (🟥)
            - Anything from ***one hour*** to ***one day***
        - Select `Jury`
            - All `Organisers` are automatically selected (🟥)
            - (*if scoring is implemented*) Users with `Ranking` `Photo Master` can also be selected, if
              the `Organisers` decide (🟨)
        - `Contest Cover Photo` (🟩)
            - A `Photo` can be selected for a `Contest`
                - **Option 1** - upload a cover photo.
                - **Option 2** - paste the URL of an existing photo.
                - **Option 3** - select a cover from previously uploaded photos.
            - The `Organiser` ***must*** be able to choose between ***all three options***
              (required if contest cover photo is implemented)

****

### Scoring (🟨)

- `Contest` participation should award `Points`. `Points` are accumulative - being invited and winning will award **53
  points total**.
    - Joining `Active / Open` `Contest` - ***1 point***
        - Invited by `Organiser` - ***3 points***
        - 3rd place - ***20 points*** (***10 points*** if shared)
        - 2nd place - ***35 points*** (***25 points*** if shared)
        - 1st place - ***50 points*** (***40 points*** if shared)
        - 1st place with **double** the `Score` of 2nd place - ***75 points***
            - Positions are shared in case of a **tie**

****

### Ranking

- (0 - 50 points) - `Photo Junkie`
- (51 - 150 points) - `Photo Enthusiast`
- (151 - 1000 points) - `Photo Master`
    - can be invited as `Jury`
- (1001 - infinity) - `Wise and Benevolent Photo Dictator`
    - can be invited as `Jury`

****

### Social Sharing (🟩)

- Participants that finish in 1st, 2nd or 3rd place in a `Contest` could have the option to share their achievement on
  social media (see Appendix).

****

## Use Cases

### New Photo Junkie

- A friend of yours sent you link to the website. You see that there is an ongoing contest about cat pics. You decide to
  participate, but the site forces you to register first. After registering successfully, you have the option to submit
  a photo to the contest.

### Being a Jury

- A user accumulated enough points to qualify as a `Jury`. The user is assigned as a `Jury` to a newly created `Contest`
  . While the `Contest` is still in `Phase I`, the juror browses through the photos, but cannot rate them yet. As soon
  as the `Contest` is switched to `Phase II` the juror can rate the submitted photos.

****

## Validation

- Some validation rules include, but are not limited to:
    - `Users`:
        - `First Name` `Last Name` - no less than 2 characters, no more than 20
        - `Username` - unique in the system
        - `Password` - at least 8 characters
    - `Contests`:
        - `Contest Title` - unique, between 5 and 50 symbols
        - `Contest Category` - one of predefined set of values (Animals, Nature, etc.)
        - `Time Limits` - must be in the future

****

## Deliverables

Provide a link to a GitLab repository with the following information in the README.md file:

- Link to the Trello board

- Link to the Swagger documentation

- Link to the hosted project ***(if hosted online)***

- Documentation describing how to build and run the project (see Spring Initializr’s own)

- Documentation how to create and fill the database with data

- Images of the database relations

****

## Technical Requirements

### General

• Use `Trello` for project management.

• Use `Git` for source control management.

• Follow the good practices about writing Git commit messages.

• Implement `Swagger` to document you API.

• Create ***user documentation / manual*** of the application.

****

### Database

• Use `MariaDB` as the database engine.

• Make sure the relations in the database are normalized.

****

### Backend

• Use `Hibernate` to access your database.

• Follow OOP’s best practices and the `SOLID principles`.

• Implement a security mechanism for managing users and roles.

• Provide a `REST API` to access the data and functionality of the project.

• Follow the best practices when designing a `REST API` (see Appendix).

• The "business" functionality should be tested properly

> At least 80% code coverage.

• Apply error handling and data validation to avoid crashes when invalid data is entered.

****

### Front-end

• For the UI you must use `Spring MVC` with `Thymeleaf` as the template engine.

• Apply error handling and data validation to avoid crashes when invalid data is entered.

• You may use `Bootstrap` or another `CSS framework`.

• You may use a free template.

****

## Optional Requirements

• Use a branching strategy when working with Git.

• Implement pagination to handle displaying large amounts of data.

• Integrate your app with a Continuous Integration server (e.g., GitLab’s own)
and have your unit tests to run on each commit to the master branch.

• Host your application’s backend in a public hosting provider of your choice (e.g., Heroku).

• Host your application’s database server in a public hosting provider of your choice.

****

## Expectations

> You must understand the system you have created.

- **It is OK** if your application has flaws or is missing a couple of must's.


- What's **NOT OK** is if you do not know what is working and what is not. That said, you should be aware of any defects
  or incomplete functionality must be properly documented and secured.

#### Some things you need to be able to explain during your project defense:

• What are the most important things you have learned while working on this project?

• What are the worst "hacks" in the project, or where do you think it needs improvement?

• What more would you do if you had another week to work on the system?

• What would you do differently if you were implementing the system again?

****

## Appendix A

• [Guidelines for designing good REST API](https://florimond.dev/blog/articles/2018/08/restful-api-design-13-best-practices-to-make-your-users-happy/)

• [Guidelines for URL encoding](http://www.talisman.org/~erlkonig/misc/lunatech%5Ewhat-every-webdev-must-know-about-url-encoding/)

• [Always prefer constructor injection](https://www.vojtechruzicka.com/field-dependency-injection-considered-harmful/)

• [Git commits - an effective style guide](https://dev.to/pavlosisaris/git-commits-an-effective-style-guide-2kkn)

• [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)

• [Facebook Social Sharing](https://developers.facebook.com/docs/sharing/web/)

****

## Appendix B

• [Swagger API Documentation Guide](https://www.dariawan.com/tutorials/spring/documenting-spring-boot-rest-api-swagger/)

• [Spring Boot File Upload](https://www.codejava.net/frameworks/spring-boot/spring-boot-file-upload-tutorial)